# Object schema

# --- !Ups

CREATE TABLE Object (
    uuid text NOT NULL,
    type text NOT NULL,
    prop jsonb NOT NULL,
    primary key (uuid)
);

# --- !Downs

DROP TABLE Object;
