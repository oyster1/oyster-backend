name := "oyster"
organization := "eu.jemelik.oyster"

version := "1.0-SNAPSHOT"

maintainer := "Hynek Jemelik"
packageSummary := "Oyster"
packageDescription := "Oyster server package"
serverLoading in Debian := com.typesafe.sbt.packager.archetypes.ServerLoader.Systemd

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.11"
scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

libraryDependencies += filters
libraryDependencies += jdbc
libraryDependencies += evolutions
libraryDependencies += "org.postgresql" % "postgresql" % "9.4-1206-jdbc42"
libraryDependencies += "org.mindrot" % "jbcrypt" % "0.4"
libraryDependencies += "com.typesafe.play" % "play-json_2.11" % "2.5.12"
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "2.0.0" % Test

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "eu.jemelik.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "eu.jemelik.binders._"


javaOptions in Universal ++= Seq(
  "-J-Xmx800m",
  "-J-Xms100m",

  s"-Dpidfile.path=/dev/null",
  "-DapplyEvolutions.default=true"
)
