package controllers

import javax.inject.Inject
import models.DatabaseObject
import models.objects.Invite
import models.objects.Token
import models.objects.User
import models.properties.LoginCredential
import play.api.db.Database
import play.api.mvc.Action
import views.UuidView
import views.TypeToUuidView
import views.Views.UuidPropertiesActionsView


class UsersController @Inject() (implicit db: Database) extends OysterController {

  def signUp = Action(parse.json) { request =>
    getParam[String](request, "email") { email =>
      getParam[String](request, "password") { password =>
        getParam[String](request, "name") { name =>
          getParam[String](request, "surname") { surname =>
            getAndLoad(request, Invite, "invite") { invite =>
              invite.accountList.getAccount.fold(Errors.InvalidParameter("invite")) { account =>
                implicit val user = DatabaseObject.create(User).asOysterObject
                user.loginCredential.changeEmail(email).fold(Errors.EmailAlreadyUsed) { _ =>
                  user.loginCredential.changePassword(password)
                  user.fullyNamed.rename(name)
                  user.fullyNamed.setSurname(surname)
                  user.accountList.addLast(account)
                  user.asDatabaseObject.save()
                  val token = DatabaseObject.create(Token).asOysterObject
                  token.authenticates.addLast(user)
                  token.asDatabaseObject.save()
                  Ok(UuidView(user) ++ TypeToUuidView(token))
                }
              }
            }
          }
        }
      }
    }
  }

  def login = Action(parse.json) { request =>
    getParam[String](request, "email") { email =>
      getParam[String](request, "password") { password =>
        LoginCredential.findByEmail(email).flatMap(_.asUser).fold(Errors.ObjectDoesNotExist) { implicit user =>
          user.loginCredential.checkPassword(password).fold(Errors.IncorrectCredentials) { _ =>
            val token = DatabaseObject.create(Token).asOysterObject
            token.authenticates.addLast(user)
            token.asDatabaseObject.save()
            Ok(UuidView(user) ++ TypeToUuidView(token))
          }
        }
      }
    }
  }

  def logout = Action { request =>
    getTokenFromRequest(request).fold(Errors.TokenAlredyInvalid) { token =>
      token.asDatabaseObject.delete()
      NoContent
    }
  }

  def me = Authenticated { request =>
    implicit val user: User = request.user
    Ok(UuidPropertiesActionsView(user))
  }

}
