package controllers

import java.time.LocalDate

import javax.inject.Inject
import models.DatabaseObject
import models.objects.Task
import models.objects.User
import models.properties.TaskList
import play.api.db.Database
import views.Views.UuidPropertiesActionsView

class TaskListController @Inject() (implicit db: Database) extends OysterController {

  def createTask(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    implicit val user: User = request.user
    loadObjectAndDoAction(objectTypeString, uuid, TaskList, user) { taskList =>
      val task = DatabaseObject.create(Task).asOysterObject
      getOptParam[String](request, "name").foreach(task.named.rename)
      getOptParam[String](request, "brief").foreach(task.brief.setBrief)
      getOptParam[LocalDate](request, "deadline").foreach(task.deadline.setDeadline)
      task.asDatabaseObject.save()
      taskList.addLast(task)
      taskList.asDatabaseObject.save()
      Created(UuidPropertiesActionsView(task))
    }
  }

  def showTasks(objectTypeString: String, uuid: String) = Authenticated { request =>
    implicit val user: User = request.user
    loadObject(objectTypeString, uuid, TaskList) { taskList =>
      Ok(UuidPropertiesActionsView(taskList)
        ++ taskList.toJson(_.toJsonUuidPropertiesActions(user)))
    }
  }

}
