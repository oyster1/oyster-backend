package controllers

import javax.inject.Inject
import models.DatabaseObject
import models.objects.User
import play.api.db.Database
import views.Views.UuidPropertiesActionsView

class ObjectsController @Inject() (implicit db: Database) extends OysterController {

  @deprecated("use object specific endpoint", "2017-06-09")
  def create(objectTypeString: String) = Authenticated { request =>
    implicit val user = request.user
    parseType(objectTypeString) { objectType =>
      val oysterObject = DatabaseObject.create(objectType).asOysterObject

      // When creating a Group
      oysterObject.asGroup.foreach { group =>
        // set an account
        request.user.accountList.getAccount.foreach { account =>
          group.accountList.addLast(account)
          account.asDatabaseObject.save()
        }
      }

      // When creating a project
      oysterObject.asProject.foreach { project =>
        // set current user as an owner
        project.ownable.addLast(request.user.owner)
        // set an account
        request.user.accountList.getAccount.foreach { account =>
          project.accountList.addLast(account)
          account.asDatabaseObject.save()
        }
      }

      oysterObject.asDatabaseObject.save()
      Created(UuidPropertiesActionsView(oysterObject))

    }
  }

  def view(objectTypeString: String, uuid: String) = Authenticated { request =>
    implicit val user: User = request.user
    loadObject(objectTypeString, uuid) { oysterObject =>
      Ok(UuidPropertiesActionsView(oysterObject))
    }
  }
}
