package controllers

import javax.inject.Inject
import models.DatabaseObject
import models.objects.Comment
import models.objects.User
import models.properties.Commentable
import play.api.db.Database
import play.api.libs.json.JsPath
import views.Views.UuidPropertiesActionsView

class CommentsController @Inject() (implicit db: Database) extends OysterController {

  def createComment(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    implicit val user: User = request.user
    // TODO: implement acl for commentable
    loadObject(objectTypeString, uuid, Commentable) { commentable =>
      getParam[String](request, "brief") { brief =>
        val comment = DatabaseObject.create(Comment).asOysterObject
        comment.brief.setBrief(brief)
        comment.ownable.addLast(request.user.owner)
        comment.asDatabaseObject.save()
        commentable.addLast(comment)
        commentable.asDatabaseObject.save()
        Created(UuidPropertiesActionsView(comment))
      }
    }
  }

  def showComments(objectTypeString: String, uuid: String) = Authenticated { request =>
    implicit val user: User = request.user
    loadObject(objectTypeString, uuid, Commentable) { commentable =>
      Ok(UuidPropertiesActionsView(commentable) ++ commentable.toJson(_.toJsonUuidProperties))
    }
  }

}
