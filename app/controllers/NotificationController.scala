package controllers

import java.time.Instant
import java.time.ZoneOffset

import javax.inject.Inject
import models.objects.Notification
import models.objects.User
import models.properties.Ownable
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import views.Views.UuidPropertiesActionsView

class NotificationController @Inject() (implicit db: Database) extends OysterController {

  def showNotifications(count: Option[Int]) = Authenticated { request =>
    implicit val user: User = request.user
    val notifications = Ownable.getReferiesFor(user.owner).flatMap(_.asNotification)
    val lastN = notifications.sortBy(_.creatable.getCreatedAt.toInstant(ZoneOffset.UTC))(
      Ordering[Instant].reverse).take(count.getOrElse(10))
    def showNotification(notification: Notification): JsObject = {
      UuidPropertiesActionsView(notification) ++
        notification.authorable.toJson(_.toJsonUuidProperties)
    }
    Ok(JsArray(lastN.map(showNotification)))
  }

}
