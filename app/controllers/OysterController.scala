package controllers

import javax.inject.Inject
import models.DatabaseObject
import models.ObjectOrProperty
import models.ObjectOrPropertyType
import models.ObjectType
import models.OysterObject
import models.Property
import models.PropertyType
import models.objects.Account
import models.objects.Notification
import models.objects.Token
import models.objects.User
import models.properties.Ownable
import play.api.db.Database
import play.api.libs.json.JsPath
import play.api.libs.json.JsValue
import play.api.libs.json.Reads
import play.api.mvc.Controller
import play.api.mvc.Request
import play.api.mvc.RequestHeader
import play.api.mvc.Result
import play.api.mvc.Security.AuthenticatedBuilder

abstract class OysterController @Inject() (implicit db: Database) extends Controller {

  protected def loadObjectAndDoAction[T <: Property](
    objectTypeString: String, uuid: String, expected: PropertyType[T], user: User)(
      onSuccess: T => Result): Result = {
    loadObject(objectTypeString, uuid, expected)(o => doAction(o, user)(onSuccess(o)))
  }

  protected def doAction(property: Property, user: User)(onSuccess: => Result): Result = {
    if (property.canDoAction(user)) {
      onSuccess
    } else {
      // TODO: consider returning action name
      Errors.NotEnoughPermissions
    }
  }

  protected def loadObject[T <: ObjectOrProperty](
    objectTypeString: String, uuid: String, expected: ObjectOrPropertyType[T])(
      onSuccess: T => Result): Result = {
    loadObject(objectTypeString, uuid)(objectAs(_, expected)(onSuccess))
  }

  protected def loadObject(objectTypeString: String, uuid: String)(
    onSuccess: OysterObject => Result): Result = {
    parseType(objectTypeString)(loadObject(_, uuid)(onSuccess))
  }

  protected def loadObject[T <: ObjectOrProperty](
    objectType: ObjectOrPropertyType[T], uuid: String)(onSuccess: T => Result): Result = {
    val oysterObject = DatabaseObject.load(uuid).map(_.asOysterObject).flatMap(_.as(objectType))
    oysterObject.fold(Errors.ObjectDoesNotExist)(onSuccess)
  }

  protected def parseType(objectTypeString: String)(
    onSuccess: ObjectType[OysterObject] => Result): Result = {
    val objectType = ObjectType.fromString(objectTypeString)
    objectType.fold(Errors.UnknownObjectType)(onSuccess)
  }

  protected def objectAs[T <: ObjectOrProperty](
    objectOrProperty: ObjectOrProperty, expected: ObjectOrPropertyType[T])(
      onSuccess: T => Result): Result = {
    objectOrProperty.as(expected).fold(Errors.MethodNotAllowed(expected.TYPE))(onSuccess)
  }

  protected def getParam[T](request: Request[JsValue], param: String)(onSuccess: T => Result)(
    implicit reads: Reads[T]): Result = {
    getOptParam[T](request, param).fold(Errors.MissingParameter(param))(onSuccess)
  }

  protected def getOptParam[T](request: Request[JsValue], param: String)(
    implicit reads: Reads[T]): Option[T] = {
    request.body.asOpt[T]((JsPath \ param).read[T])
  }

  protected def getNullableParam[T](request: Request[JsValue], param: String)(
    onSuccess: Option[T] => Result)(implicit reads: Reads[T]): Result = {
    val nullableParam = request.body.asOpt[Option[T]]((JsPath \ param).readNullable[T])
    nullableParam.fold(Errors.MissingParameter(param))(onSuccess)
  }

  protected def getAndLoad[T <: ObjectOrProperty](
    request: Request[JsValue], objectType: ObjectOrPropertyType[T], param: String)(
      onSuccess: T => Result): Result = {
    getParam[String](request, param)(loadObject(objectType, _)(onSuccess))
  }

  protected def notifyOwners(name: String, author: User)(ownable: Ownable) {
    val owners = ownable.getReferencedObjects
    owners.foreach { owner =>
      if (owner.uuid != author.uuid) { // Skip notifying the current user
        val notification = DatabaseObject.create(Notification).asOysterObject
        notification.ownable.addLast(owner)
        notification.authorable.addLast(author)
        notification.named.rename(name)
        notification.objectList.addLast(ownable)
        notification.asDatabaseObject.save()
      }
    }
  }

  protected def getTokenFromRequest(request: RequestHeader): Option[Token] = {
    val header = request.headers.get("Authorization-Token")
    val token = header.flatMap(DatabaseObject.load).map(_.asOysterObject).flatMap(_.asToken)
    token
  }

  private def getUserFromRequest(request: RequestHeader): Option[User] = {
    val token = getTokenFromRequest(request)
    val user = token.flatMap(_.authenticates.getUser)
    user
  }

  object Authenticated extends AuthenticatedBuilder(getUserFromRequest, _ => Errors.SignInRequired)

  protected def getAccount(user: User)(onSuccess: Account => Result): Result = {
    user.accountList.getAccount.fold(Errors.NotEnoughPermissions)(onSuccess)
  }
}
