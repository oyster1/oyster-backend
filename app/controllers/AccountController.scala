package controllers

import java.time.LocalDate

import javax.inject.Inject
import models.DatabaseObject
import models.objects.Account
import models.objects.Group
import models.objects.Invite
import models.objects.Project
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.mvc.Action
import views.TypeToUuidView
import views.Views.UuidPropertiesActionsView
import views.Views.UuidPropertiesView

class AccountController @Inject() (implicit db: Database) extends OysterController {

  def createAccount = Action(parse.json) { request =>
    getParam[String](request, "name") { name =>
      val account = DatabaseObject.create(Account).asOysterObject
      account.named.rename(name)
      val invite = DatabaseObject.create(Invite).asOysterObject
      invite.accountList.addLast(account)
      account.asDatabaseObject.save()
      invite.asDatabaseObject.save()
      Created(account.toJsonUuidProperties ++ invite.toJsonTypeToUuid)
    }
  }

  def mine = Authenticated { request =>
    implicit val user: User = request.user
    getAccount(request.user) { account =>
      Ok(UuidPropertiesView(account))
    }
  }

  def createInvite = Authenticated { request =>
    implicit val user: User = request.user
    getAccount(user) { account =>
      val invite = DatabaseObject.create(Invite).asOysterObject
      invite.accountList.addLast(account)
      invite.asDatabaseObject.save()
      Created(TypeToUuidView(invite))
    }
  }

  def users = Authenticated { request =>
    implicit val user: User = request.user
    getAccount(user) { account =>
      Ok(JsArray(account.getAllUsers.map(UuidPropertiesView.apply[User])))
    }
  }

  def groups = Authenticated { request =>
    implicit val user: User = request.user
    getAccount(user) { account =>
      Ok(JsArray(account.getAllGroups.map(UuidPropertiesActionsView.apply[Group])))
    }
  }

  def createProject = Authenticated(parse.json) { request =>
    implicit val user: User = request.user
    getAccount(user) { account =>
      val project = DatabaseObject.create(Project).asOysterObject
      getOptParam[String](request, "name").foreach(project.named.rename)
      getOptParam[String](request, "brief").foreach(project.brief.setBrief)
      getOptParam[LocalDate](request, "deadline").foreach(project.deadline.setDeadline)
      project.ownable.addLast(request.user.owner)
      project.accountList.addLast(account)
      project.asDatabaseObject.save()
      account.asDatabaseObject.save()
      Created(UuidPropertiesActionsView(project))
    }
  }

  def projects = Authenticated { request =>
    implicit val user: User = request.user
    getAccount(user) { account =>
      val projects = account.getAllProjects.filter(_.archivable.isVisible)
      def projectWithTasksExpanded(project: Project): JsObject = {
        UuidPropertiesActionsView(project) ++
          project.taskList.toJsonTasks(_.toJsonUuidPropertiesActions(request.user))
      }
      Ok(JsArray(projects.map(projectWithTasksExpanded)))
    }
  }
}
