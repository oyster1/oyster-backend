package controllers

import Errors.UnsupportedAction
import javax.inject.Inject
import models.DatabaseObject
import models.properties.Owner
import models.OysterObject
import models.objects.Project
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.Json

class ViewController @Inject() (implicit db: Database) extends OysterController {

  def ownerProjectList(objectTypeString: String, uuid: String) = Authenticated { request =>
    loadObject(objectTypeString, uuid) { oysterObject =>
      oysterObject.asOwner.fold(UnsupportedAction("owned_projects")) { owner =>
        Ok(owner.toClientJson)
      }
    }
  }

  @deprecated("use account/projects instead", "2017-06-09")
  def allProjects = Authenticated {
    val projects = DatabaseObject.loadAll(Project).map(_.asOysterObject)
    Ok(JsArray(projects.map(p => p.toJsonUuid ++ p.toJsonProperties ++ p.taskList.toJsonTasks(t => t.toJsonUuid ++ t.toJsonProperties))))
  }

}
