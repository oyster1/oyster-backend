package controllers

import play.api.mvc.Results
import play.api.libs.json.JsValue
import play.api.libs.json.Json

object Errors {
  def GenericBadRequest(message: String) = Results.BadRequest(msg("bad request", message))
  def GenericForbidden(message: String) = Results.Forbidden(msg("forbidden", message))
  def GenericNotFound(message: String) = Results.NotFound(msg("not found", message))
  def GenericClientError(message: String) = Results.BadRequest(msg("client error", message))
  def GenericServerError(message: String) = Results.InternalServerError(msg("server error", message))

  val IncorrectCredentials = Results.Unauthorized(msg("provided credentials are incorrect"))
  val SignInRequired = Results.Unauthorized(msg("you need to sign in to access this page"))
  val NotEnoughPermissions = Results.Forbidden(msg("you do not have enough permissions to view this page"))
  val UnknownObjectType = Results.BadRequest(msg("unknown object type"))
  val ObjectDoesNotExist = Results.NotFound(msg("object does not exist"))
  val TokenAlredyInvalid = Results.Gone(msg("this token is already invalid"))
  def MethodNotAllowed(message: String) = Results.MethodNotAllowed(msg("object is not", message))
  val EmailAlreadyUsed = Results.Conflict(msg("email is already used by someone else"))
  val ProjectMustHaveManager = Results.Conflict(msg("project must have at least one manager"))
  def AlreadyExists(value: String) = Results.BadRequest(msg("object already exists", value))
  def UnsupportedAction(action: String) = Results.MethodNotAllowed(msg("unsupported action", action))
  def MissingParameter(parameter: String) = Results.BadRequest(msg("missing parameter", parameter))
  def InvalidParameter(parameter: String) = Results.BadRequest(msg("invalid parameter", parameter))

  private def msg(errorName: String, message: String = ""): JsValue = {
    val error = if (message.length > 0) errorName + ": " + message else errorName
    Json.toJson(Map("error" -> error))
  }
}
