package controllers

import javax.inject.Inject
import models.properties.Archivable
import models.properties.ProjectList
import models.properties.TaskList
import play.api.db.Database

class ArchivableController @Inject() (implicit db: Database) extends OysterController {

  def archive(objectTypeString: String, uuid: String) = Authenticated { request =>
    loadObject(objectTypeString, uuid, Archivable) { archivable =>

      // TODO: Consider removing all references at once

      archivable.asTask.foreach { task =>
        TaskList.getReferiesFor(task).flatMap(_.asTaskList).foreach { taskList =>
          taskList.remove(task)
          taskList.asDatabaseObject.save()
        }
      }

      archivable.asProject.foreach { project =>
        ProjectList.getReferiesFor(project).flatMap(_.asProjectList).foreach { projectList =>
          projectList.remove(project)
          projectList.asDatabaseObject.save()
        }
      }

      archivable.archive()
      archivable.asDatabaseObject.save()
      Ok(archivable.toClientJson)
    }
  }

}
