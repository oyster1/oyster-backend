package controllers

import java.time.LocalDate

import Errors.InvalidParameter
import Errors.MissingParameter
import Errors.UnsupportedAction
import javax.inject.Inject
import models.DatabaseObject
import models.properties.Approvable
import models.properties.Brief
import models.properties.Completable
import models.properties.Deadline
import models.properties.Named
import models.properties.Ownable
import models.properties.Owner
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.JsPath

class ActionController @Inject() (implicit db: Database) extends OysterController {

  def rename(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObjectAndDoAction(objectTypeString, uuid, Named, request.user) { named =>
      // Consider alowing change of surname without updating name
      getParam[String](request, "name") { newName =>
        getOptParam[String](request, "surname").foreach { newSurname =>
          // TODO: consider returning an error when surname is present for non FullyNamed object
          named.asFullyNamed.foreach { fullyNamed =>
            fullyNamed.setSurname(newSurname)
          }
        }
        named.rename(newName)
        named.asDatabaseObject.save()
        named.asOwnable.foreach(notifyOwners(Named.ACTION, request.user))
        Ok(named.toClientJson)
      }
    }
  }

  def deadline(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObjectAndDoAction(objectTypeString, uuid, Deadline, request.user) { deadline =>
      getNullableParam[LocalDate](request, "deadline") { nullableDeadline =>
        nullableDeadline.fold(deadline.removeDeadline)(deadline.setDeadline)
        deadline.asDatabaseObject.save()
        deadline.asOwnable.foreach(notifyOwners(Deadline.ACTION, request.user))
        Ok(deadline.toClientJson)
      }
    }
  }

  def brief(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObjectAndDoAction(objectTypeString, uuid, Brief, request.user) { brief =>
      getParam[String](request, "brief") { newBrief =>
        brief.setBrief(newBrief)
        brief.asDatabaseObject.save()
        brief.asOwnable.foreach(notifyOwners(Brief.ACTION, request.user))
        Ok(brief.toClientJson)
      }
    }
  }

  def assign(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObjectAndDoAction(objectTypeString, uuid, Ownable, request.user) { ownable =>
      getAndLoad(request, Owner, "owner") { owner =>
        ownable.addLast(owner)
        ownable.asDatabaseObject.save()
        ownable.asOwnable.foreach(notifyOwners(Ownable.ACTION, request.user))
        Ok(ownable.toClientJson)
      }
    }
  }

  def unassign(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObjectAndDoAction(objectTypeString, uuid, Ownable, request.user) { ownable =>
      getAndLoad(request, Owner, "owner") { owner =>
        ownable.remove(owner)
        // TODO: this is mega ugly, consider adding constrain validation to References
        if (ownable.asProject.isDefined && ownable.getUuids.isEmpty) {
          Errors.ProjectMustHaveManager
        } else {
          ownable.asDatabaseObject.save()
          Ok(ownable.toClientJson)
        }
      }
    }
  }

  def moveToProject(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObject(objectTypeString, uuid) { oysterObject =>
      oysterObject.asTask.fold(UnsupportedAction("move_to_project")) { task =>
        request.body.asOpt[String]((JsPath \ "project").read[String]).fold(MissingParameter("project")) { taskListUuid =>
          DatabaseObject.load(taskListUuid).map(_.asOysterObject).flatMap(_.asProject).fold(InvalidParameter("project")) { project =>
            project.taskList.addLast(task)
            project.asDatabaseObject.save()
            Ok(project.toJsonUuid ++ project.toJsonType)
          }
        }
      }
    }
  }

  def moveToGroup(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObject(objectTypeString, uuid) { oysterObject =>
      oysterObject.asProject.fold(UnsupportedAction("move_to_group")) { project =>
        request.body.asOpt[String]((JsPath \ "group").read[String]).fold(MissingParameter("group")) { groupUuid =>
          DatabaseObject.load(groupUuid).map(_.asOysterObject).flatMap(_.asGroup).fold(InvalidParameter("group")) { group =>
            group.projectList.addFirst(project)
            group.asDatabaseObject.save()
            Ok(group.toJsonUuid ++ group.toJsonType)
          }
        }
      }
    }
  }

  def complete(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObjectAndDoAction(objectTypeString, uuid, Completable, request.user) { completable =>
      completable.complete()
      completable.asDatabaseObject.save()
      Ok(completable.toClientJson)
    }
  }

  def reopen(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObjectAndDoAction(objectTypeString, uuid, Completable, request.user) { completable =>
      completable.reopen()
      // Reopening completable also causes any pending approval to be rejected
      completable.asApprovable.foreach(_.reject())
      completable.asDatabaseObject.save()
      Ok(completable.toClientJson)
    }
  }

  def approve(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObjectAndDoAction(objectTypeString, uuid, Approvable, request.user) { approvable =>
      approvable.approve()
      approvable.asDatabaseObject.save()
      approvable.asOwnable.foreach(notifyOwners(Approvable.APPROVE_ACTION, request.user))
      Ok(approvable.toClientJson)
    }
  }

  def reject(objectTypeString: String, uuid: String) = Authenticated(parse.json) { request =>
    loadObjectAndDoAction(objectTypeString, uuid, Approvable, request.user) { approvable =>
      approvable.reject()
      // Rejecting approval also causes the underlying completable to reopen
      approvable.asCompletable.foreach(_.reopen())
      approvable.asDatabaseObject.save()
      approvable.asOwnable.foreach(notifyOwners(Approvable.REJECT_ACTION, request.user))
      Ok(approvable.toClientJson)
    }
  }

  def assignables(uuid: String) = Authenticated {
    DatabaseObject.load(uuid).map(_.asOysterObject).fold(NotFound("object does not exist")) { oysterObject =>
      oysterObject.asOwner.fold(NotFound("this object type cannot have assignables")) { owner =>
        Ok(JsArray(owner.getAssignedObjects.map(a => a.toJsonType ++ a.toJsonUuid)))
      }
    }
  }

}
