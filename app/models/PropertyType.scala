package models

import models.properties.AccountList
import models.properties.Approvable
import models.properties.Archivable
import models.properties.Authenticates
import models.properties.Authorable
import models.properties.Brief
import models.properties.Commentable
import models.properties.Completable
import models.properties.Creatable
import models.properties.Deadline
import models.properties.LoginCredential
import models.properties.Named
import models.properties.ObjectList
import models.properties.Ownable
import models.properties.Owner
import models.properties.ProjectList
import models.properties.TaskList
import play.api.libs.json.JsValue

abstract class PropertyType[+T <: Property] extends ObjectOrPropertyType[T] {
  def create(parent: => OysterObject, json: JsValue): T
}

object PropertyType {
  val ALL: Vector[PropertyType[Property]] = Vector(
    AccountList,
    Approvable,
    Archivable,
    Authenticates,
    Authorable,
    Brief,
    Commentable,
    Completable,
    Creatable,
    Deadline,
    LoginCredential,
    Named,
    ObjectList,
    Ownable,
    Owner,
    ProjectList,
    TaskList)
}
