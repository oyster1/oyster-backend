package models

import models.objects.Account
import models.objects.Comment
import models.objects.Group
import models.objects.Invite
import models.objects.Notification
import models.objects.Project
import models.objects.Task
import models.objects.Token
import models.objects.User
import play.api.libs.json.JsValue

abstract class ObjectType[+T <: OysterObject] extends ObjectOrPropertyType[T] {
  def createObject(uuid: String, prop: JsValue): T
}

object ObjectType {
  def fromString(objectTypeString: String): Option[ObjectType[OysterObject]] = {
    objectTypeString match {
      case Account.TYPE      => Some(Account)
      // TODO: add attachment here
      case Comment.TYPE      => Some(Comment)
      case Group.TYPE        => Some(Group)
      case Invite.TYPE       => Some(Invite)
      case Notification.TYPE => Some(Notification)
      case Project.TYPE      => Some(Project)
      case Task.TYPE         => Some(Task)
      case Token.TYPE        => Some(Token)
      case User.TYPE         => Some(User)
      case _                 => None
    }
  }
}
