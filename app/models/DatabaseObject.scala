package models

import java.sql.ResultSet
import java.sql.Types
import java.util.UUID

import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import play.api.libs.json.Json

case class DatabaseObject[+T <: OysterObject](
    uuid: String, objectType: ObjectType[T], prop: JsValue) {

  def asOysterObject: T = objectType.createObject(uuid, prop)

  def save()(implicit db: Database) {
    db.withConnection { conn =>
      val stmt = conn.prepareStatement(DatabaseObject.saveSql)
      stmt.setString(1, uuid)
      stmt.setString(2, objectType.TYPE)
      stmt.setObject(3, prop, Types.OTHER)
      stmt.executeUpdate()
    }
  }

  def delete()(implicit db: Database) {
    db.withConnection { conn =>
      val stmt = conn.prepareStatement(DatabaseObject.deleteSql)
      stmt.setString(1, uuid)
      stmt.setString(2, objectType.TYPE)
      stmt.executeUpdate()
    }
  }
}

object DatabaseObject {
  def create[T <: OysterObject](objectType: ObjectType[T]): DatabaseObject[T] = {
    val uuid = UUID.randomUUID.toString
    val emptyJsObject = JsObject.apply(Seq())
    DatabaseObject(uuid, objectType, emptyJsObject)
  }

  def createFromResultSet(rs: ResultSet): Option[DatabaseObject[OysterObject]] = {
    ObjectType.fromString(rs.getString("type")).map(DatabaseObject(rs.getString("uuid"), _,
      Json.parse(rs.getString("prop"))))
  }

  def createFromResultSet[T <: OysterObject](
    rs: ResultSet, expectedType: ObjectType[T]): Option[DatabaseObject[T]] = {

    ObjectType.fromString(rs.getString("type")).filter(_.TYPE == expectedType.TYPE).map { _ =>
      DatabaseObject(rs.getString("uuid"), expectedType, Json.parse(rs.getString("prop")))
    }
  }

  def load(uuid: String)(implicit db: Database): Option[DatabaseObject[OysterObject]] = {
    db.withConnection { conn =>
      val stmt = conn.prepareStatement(selectSql)
      stmt.setString(1, uuid)
      val rs = stmt.executeQuery()
      if (rs.next()) {
        createFromResultSet(rs)
      } else {
        None
      }
    }
  }

  def loadAll[T <: OysterObject](objectType: ObjectType[T])(
    implicit db: Database): Vector[DatabaseObject[T]] = {

    val resultVector = Vector.newBuilder[DatabaseObject[T]]
    db.withConnection { conn =>
      val stmt = conn.prepareStatement(selectAllSql)
      stmt.setString(1, objectType.TYPE)
      val rs = stmt.executeQuery()
      while (rs.next()) {
        DatabaseObject.createFromResultSet(rs, objectType).foreach {
          resultVector += _
        }
      }
    }
    resultVector.result()
  }

  def loadByProp(propName: String, propValue: String)(
    implicit db: Database): Vector[DatabaseObject[OysterObject]] = {

    val resultVector = Vector.newBuilder[DatabaseObject[OysterObject]]
    db.withConnection { conn =>
      val stmt = conn.prepareStatement(selectByPropSql)
      stmt.setString(1, propName)
      stmt.setString(2, propValue)
      val rs = stmt.executeQuery()
      while (rs.next()) {
        DatabaseObject.createFromResultSet(rs).foreach {
          resultVector += _
        }
      }
    }
    resultVector.result()
  }

  def loadReferies[T <: ObjectOrProperty](propName: String, owner: T)(
    implicit db: Database): Vector[DatabaseObject[OysterObject]] = {

    val resultVector = Vector.newBuilder[DatabaseObject[OysterObject]]
    db.withConnection { conn =>
      val stmt = conn.prepareStatement(selectReffereesSql)
      stmt.setString(1, propName)
      stmt.setString(2, owner.uuid)
      val rs = stmt.executeQuery()
      while (rs.next()) {
        DatabaseObject.createFromResultSet(rs).foreach {
          resultVector += _
        }
      }
    }
    resultVector.result()
  }

  private val selectSql = """
    SELECT uuid, type, prop
    FROM Object
    WHERE uuid = ?"""

  private val selectAllSql = """
    SELECT uuid, type, prop
    FROM Object
    WHERE type = ?"""

  private val selectByPropSql = """
    SELECT uuid, type, prop
    FROM Object
    WHERE prop->>? = ?"""

  private val selectReffereesSql = """
    SELECT uuid, type, prop
    FROM Object
    WHERE prop->? ?? ?"""

  private val saveSql = """
    INSERT INTO Object (uuid, type, prop)
    VALUES (?, ?, ?)
    ON CONFLICT (uuid)
    DO UPDATE SET type = EXCLUDED.type, prop = EXCLUDED.prop"""

  private val deleteSql = """
    DELETE
    FROM Object
    WHERE uuid = ? AND type = ?"""
}
