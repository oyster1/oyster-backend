package models

import models.objects.Account
import models.objects.Attachment
import models.objects.Comment
import models.objects.Group
import models.objects.Invite
import models.objects.Notification
import models.objects.Project
import models.objects.Task
import models.objects.Token
import models.objects.User
import models.properties.AccountList
import models.properties.Approvable
import models.properties.Archivable
import models.properties.Authenticates
import models.properties.Authorable
import models.properties.Brief
import models.properties.Commentable
import models.properties.Completable
import models.properties.Creatable
import models.properties.Deadline
import models.properties.FullyNamed
import models.properties.LoginCredential
import models.properties.Named
import models.properties.ObjectList
import models.properties.Ownable
import models.properties.Owner
import models.properties.ProjectList
import models.properties.TaskList
import models.properties.Url
import play.api.db.Database
import play.api.libs.json.JsObject

abstract class Property(parent: => OysterObject) extends ObjectOrProperty {

  def toDatabaseJson: JsObject
  def toClientJson(implicit db: Database): JsObject = toDatabaseJson
  def accept[T](visitor: PropertyVisitor[T]): T
  def canDoAction(user: User)(implicit db: Database): Boolean = false
  def getActionNames: Vector[String] = Vector.empty

  def uuid: String = parent.uuid
  def objectType: ObjectType[OysterObject] = parent.objectType

  def asAccount: Option[Account] = parent.asAccount
  def asAttachment: Option[Attachment] = parent.asAttachment
  def asComment: Option[Comment] = parent.asComment
  def asGroup: Option[Group] = parent.asGroup
  def asInvite: Option[Invite] = parent.asInvite
  def asNotification: Option[Notification] = parent.asNotification
  def asProject: Option[Project] = parent.asProject
  def asTask: Option[Task] = parent.asTask
  def asToken: Option[Token] = parent.asToken
  def asUser: Option[User] = parent.asUser

  def asAccountList: Option[AccountList] = parent.asAccountList
  def asApprovable: Option[Approvable] = parent.asApprovable
  def asArchivable: Option[Archivable] = parent.asArchivable
  def asAuthenticates: Option[Authenticates] = parent.asAuthenticates
  def asAuthorable: Option[Authorable] = parent.asAuthorable
  def asBrief: Option[Brief] = parent.asBrief
  def asCommentable: Option[Commentable] = parent.asCommentable
  def asCompletable: Option[Completable] = parent.asCompletable
  def asCreatable: Option[Creatable] = parent.asCreatable
  def asDeadline: Option[Deadline] = parent.asDeadline
  def asFullyNamed: Option[FullyNamed] = parent.asFullyNamed
  def asLoginCredential: Option[LoginCredential] = parent.asLoginCredential
  def asNamed: Option[Named] = parent.asNamed
  def asObjectList: Option[ObjectList] = parent.asObjectList
  def asOwnable: Option[Ownable] = parent.asOwnable
  def asOwner: Option[Owner] = parent.asOwner
  def asProjectList: Option[ProjectList] = parent.asProjectList
  def asTaskList: Option[TaskList] = parent.asTaskList
  def asUrl: Option[Url] = parent.asUrl

}
