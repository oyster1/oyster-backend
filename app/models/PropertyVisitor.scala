package models

import models.properties.AccountList
import models.properties.Approvable
import models.properties.Archivable
import models.properties.Authenticates
import models.properties.Authorable
import models.properties.Brief
import models.properties.Commentable
import models.properties.Completable
import models.properties.Creatable
import models.properties.Deadline
import models.properties.LoginCredential
import models.properties.Named
import models.properties.ObjectList
import models.properties.Ownable
import models.properties.Owner
import models.properties.ProjectList
import models.properties.TaskList

trait PropertyVisitor[+T] {

  def visitAccountList(accountList: AccountList): T
  def visitApprovable(approvable: Approvable): T
  def visitArchivable(archivable: Archivable): T
  def visitAuthenticates(authenticates: Authenticates): T
  def visitAuthorable(authorable: Authorable): T
  def visitBrief(brief: Brief): T
  def visitCommentable(commentable: Commentable): T
  def visitCompletable(completable: Completable): T
  def visitCreatable(creatable: Creatable): T
  def visitDeadline(deadline: Deadline): T
  // TODO: Consider if FullyNamed is needed here
  def visitLoginCredential(loginCredential: LoginCredential): T
  def visitNamed(named: Named): T
  def visitObjectList(objectList: ObjectList): T
  def visitOwnable(ownable: Ownable): T
  def visitOwner(owner: Owner): T
  def visitProjectList(projectList: ProjectList): T
  def visitTaskList(taskList: TaskList): T

}
