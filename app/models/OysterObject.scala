package models

import models.objects.Account
import models.objects.Attachment
import models.objects.Comment
import models.objects.Group
import models.objects.Invite
import models.objects.Notification
import models.objects.Project
import models.objects.Task
import models.objects.Token
import models.objects.User
import models.properties.AccountList
import models.properties.Approvable
import models.properties.Archivable
import models.properties.Authenticates
import models.properties.Authorable
import models.properties.Brief
import models.properties.Commentable
import models.properties.Completable
import models.properties.Creatable
import models.properties.Deadline
import models.properties.FullyNamed
import models.properties.LoginCredential
import models.properties.Named
import models.properties.ObjectList
import models.properties.Ownable
import models.properties.Owner
import models.properties.ProjectList
import models.properties.TaskList
import models.properties.Url
import play.api.db.Database

abstract class OysterObject extends ObjectOrProperty {

  // Please override one of these methods with Some(this)
  def asAccount: Option[Account] = None
  def asAttachment: Option[Attachment] = None
  def asComment: Option[Comment] = None
  def asGroup: Option[Group] = None
  def asInvite: Option[Invite] = None
  def asNotification: Option[Notification] = None
  def asProject: Option[Project] = None
  def asTask: Option[Task] = None
  def asToken: Option[Token] = None
  def asUser: Option[User] = None

  // Override if you implement a particular property
  def asAccountList: Option[AccountList] = None
  def asApprovable: Option[Approvable] = None
  def asArchivable: Option[Archivable] = None
  def asAuthenticates: Option[Authenticates] = None
  def asAuthorable: Option[Authorable] = None
  def asBrief: Option[Brief] = None
  def asCommentable: Option[Commentable] = None
  def asCompletable: Option[Completable] = None
  def asCreatable: Option[Creatable] = None
  def asDeadline: Option[Deadline] = None
  def asFullyNamed: Option[FullyNamed] = None
  def asLoginCredential: Option[LoginCredential] = None
  def asNamed: Option[Named] = None
  def asObjectList: Option[ObjectList] = None
  def asOwnable: Option[Ownable] = None
  def asOwner: Option[Owner] = None
  def asProjectList: Option[ProjectList] = None
  def asTaskList: Option[TaskList] = None
  def asUrl: Option[Url] = None

  // Override if more fine grained access control is needed
  def canRename(user: User)(implicit db: Database): Boolean = true
  def canChangeDeadline(user: User)(implicit db: Database): Boolean = true
  def canSetBrief(user: User)(implicit db: Database): Boolean = true
  def canComplete(user: User)(implicit db: Database): Boolean = true
  def canArchive(user: User)(implicit db: Database): Boolean = true
  def canAssign(user: User)(implicit db: Database): Boolean = true
  def canApprove(user: User)(implicit db: Database): Boolean = true
  def canAddTask(user: User)(implicit db: Database): Boolean = true

}

object OysterObject {
}
