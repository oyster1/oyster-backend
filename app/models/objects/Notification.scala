package models.objects

import models.ObjectOrProperty
import models.ObjectType
import models.OysterObject
import models.properties.Authorable
import models.properties.Completable
import models.properties.Creatable
import models.properties.Named
import models.properties.ObjectList
import models.properties.Ownable
import play.api.db.Database
import play.api.libs.json.JsValue

case class Notification(
    uuid: String,
    ownable: Ownable,
    creatable: Creatable,
    authorable: Authorable,
    named: Named,
    objectList: ObjectList,
    completable: Completable) extends OysterObject {

  override def asNotification = Some(this)
  override def asOwnable = Some(ownable)
  override def asCreatable = Some(creatable)
  override def asAuthorable = Some(authorable)
  override def asNamed = Some(named)
  override def asObjectList = Some(objectList)
  override def asCompletable = Some(completable)

  // Nobody can rename or reassign a notification
  override def canRename(user: User)(implicit db: Database): Boolean = false
  override def canAssign(user: User)(implicit db: Database): Boolean = false

  def objectType = Notification

}

object Notification extends ObjectType[Notification] {
  val TYPE = "notification"

  def createObject(uuid: String, prop: JsValue): Notification = {
    lazy val notification: Notification = new Notification(uuid,
      Ownable.create(notification, prop),
      Creatable.create(notification, prop),
      Authorable.create(notification, prop),
      Named.create(notification, prop),
      ObjectList.create(notification, prop),
      Completable.create(notification, prop))
    notification
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Notification] = {
    oysterObject.asNotification
  }
}
