package models.objects

import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import models.properties.Ownable
import models.properties.Brief
import models.ObjectOrProperty
import models.ObjectType
import models.OysterObject

case class Comment(
    uuid: String,
    ownable: Ownable,
    brief: Brief) extends OysterObject {

  override def asComment = Some(this)
  override def asOwnable = Some(ownable)
  override def asBrief = Some(brief)

  def objectType = Comment

}

object Comment extends ObjectType[Comment] {
  val TYPE = "comment"

  def createObject(uuid: String, prop: JsValue): Comment = {
    lazy val comment: Comment = new Comment(uuid, Ownable.create(comment, prop),
      Brief.create(comment, prop))
    comment
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Comment] = oysterObject.asComment
}
