package models.objects

import models.ObjectOrProperty
import models.ObjectType
import models.OysterObject
import models.properties.Approvable
import models.properties.Archivable
import models.properties.Brief
import models.properties.Commentable
import models.properties.Completable
import models.properties.Deadline
import models.properties.Named
import models.properties.Ownable
import models.properties.TaskList
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue

case class Task(
    uuid: String,
    named: Named,
    ownable: Ownable,
    brief: Brief,
    deadline: Deadline,
    completable: Completable,
    approvable: Approvable,
    commentable: Commentable,
    archivable: Archivable) extends OysterObject {

  override def asTask = Some(this)
  override def asNamed = Some(named)
  override def asOwnable = Some(ownable)
  override def asBrief = Some(brief)
  override def asDeadline = Some(deadline)
  override def asCompletable = Some(completable)
  override def asApprovable = Some(approvable)
  override def asCommentable = Some(commentable)
  override def asArchivable = Some(archivable)

  private def isOwner(user: User): Boolean = ownable.contains(user.owner)

  override def canComplete(user: User)(implicit db: Database): Boolean = isOwner(user)

  // TODO: figure out how this can be cached
  private def projects(implicit db: Database): Vector[Project] = {
    TaskList.getReferiesFor(this).flatMap(_.asProject)
  }

  private def isManager(user: User)(implicit db: Database): Boolean = {
    projects.exists(_.ownable.contains(user.owner))
  }

  override def canRename(user: User)(implicit db: Database): Boolean = isManager(user)
  override def canSetBrief(user: User)(implicit db: Database): Boolean = isManager(user)
  override def canChangeDeadline(user: User)(implicit db: Database): Boolean = isManager(user)
  override def canArchive(user: User)(implicit db: Database): Boolean = isManager(user)
  override def canAssign(user: User)(implicit db: Database): Boolean = isManager(user)
  override def canApprove(user: User)(implicit db: Database): Boolean = {
    completable.isCompleted && isManager(user)
  }

  def objectType = Task

  def toJsonProjects(implicit db: Database): JsObject = {
    val projectsUuids = projects.map(_.toJsonUuid)
    JsObject(Seq("projects" -> JsArray(projectsUuids)))
  }

}

object Task extends ObjectType[Task] {
  val TYPE = "task"

  def createObject(uuid: String, prop: JsValue): Task = {
    lazy val task: Task = Task(uuid, Named.create(task, prop), Ownable.create(task, prop),
      Brief.create(task, prop), Deadline.create(task, prop), Completable.create(task, prop),
      Approvable.create(task, prop), Commentable.create(task, prop), Archivable.create(task, prop))
    task
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Task] = oysterObject.asTask
}
