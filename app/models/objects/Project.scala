package models.objects

import models.ObjectOrProperty
import models.ObjectType
import models.OysterObject
import models.properties.AccountList
import models.properties.Archivable
import models.properties.Brief
import models.properties.Deadline
import models.properties.Named
import models.properties.Ownable
import models.properties.TaskList
import play.api.db.Database
import play.api.libs.json.JsValue

case class Project(
    uuid: String,
    named: Named,
    ownable: Ownable,
    brief: Brief,
    deadline: Deadline,
    accountList: AccountList,
    taskList: TaskList,
    archivable: Archivable) extends OysterObject {

  override def asProject = Some(this)
  override def asNamed = Some(named)
  override def asOwnable = Some(ownable)
  override def asBrief = Some(brief)
  override def asDeadline = Some(deadline)
  override def asAccountList = Some(accountList)
  override def asTaskList = Some(taskList)
  override def asArchivable = Some(archivable)

  private def isOwner(user: User): Boolean = ownable.contains(user.owner)

  override def canRename(user: User)(implicit db: Database): Boolean = isOwner(user)
  override def canSetBrief(user: User)(implicit db: Database): Boolean = isOwner(user)
  override def canChangeDeadline(user: User)(implicit db: Database): Boolean = isOwner(user)
  override def canArchive(user: User)(implicit db: Database): Boolean = isOwner(user)
  override def canAssign(user: User)(implicit db: Database): Boolean = isOwner(user)
  override def canAddTask(user: User)(implicit db: Database): Boolean = isOwner(user)

  def objectType = Project

}

object Project extends ObjectType[Project] {
  val TYPE = "project"

  def createObject(uuid: String, prop: JsValue): Project = {
    lazy val project: Project = Project(uuid, Named.create(project, prop),
      Ownable.create(project, prop), Brief.create(project, prop), Deadline.create(project, prop),
      AccountList.create(project, prop), TaskList.create(project, prop),
      Archivable.create(project, prop))
    project
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Project] = oysterObject.asProject
}
