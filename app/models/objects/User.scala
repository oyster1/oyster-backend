package models.objects

import models.ObjectOrProperty
import models.ObjectType
import models.OysterObject
import models.properties.AccountList
import models.properties.FullyNamed
import models.properties.LoginCredential
import models.properties.Named
import models.properties.Owner
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue

case class User(
    uuid: String,
    loginCredential: LoginCredential,
    accountList: AccountList,
    fullyNamed: FullyNamed,
    owner: Owner) extends OysterObject {

  override def asUser = Some(this)
  override def asLoginCredential = Some(loginCredential)
  override def asAccountList = Some(accountList)
  override def asNamed = Some[Named](fullyNamed)
  override def asFullyNamed = Some(fullyNamed)
  override def asOwner = Some(owner)

  def objectType = User

}

object User extends ObjectType[User] {
  val TYPE = "user"

  def createObject(uuid: String, prop: JsValue): User = {
    lazy val user: User = User(uuid, LoginCredential.create(user, prop),
      AccountList.create(user, prop), FullyNamed.create(user, prop), Owner.create(user, prop))
    user
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[User] = oysterObject.asUser
}
