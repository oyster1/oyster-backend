package models.objects

import models.ObjectOrProperty
import models.ObjectType
import models.OysterObject
import models.properties.AccountList
import models.properties.Named
import models.properties.Ownable
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue

// TODO: Consider adding default Project List to an account
case class Account(
    uuid: String,
    named: Named,
    ownable: Ownable) extends OysterObject {

  override def asAccount = Some(this)
  override def asNamed = Some(named)
  override def asOwnable = Some(ownable)

  def objectType = Account

  def getAllUsers(implicit db: Database): Vector[User] = {
    AccountList.getReferiesFor(this).flatMap(_.asUser)
  }

  def getAllProjects(implicit db: Database): Vector[Project] = {
    AccountList.getReferiesFor(this).flatMap(_.asProject)
  }

  def getAllGroups(implicit db: Database): Vector[Group] = {
    AccountList.getReferiesFor(this).flatMap(_.asGroup)
  }

}

object Account extends ObjectType[Account] {
  val TYPE = "account"

  def createObject(uuid: String, prop: JsValue): Account = {
    lazy val account: Account = Account(uuid, Named.create(account, prop),
      Ownable.create(account, prop))
    account
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Account] = oysterObject.asAccount

}
