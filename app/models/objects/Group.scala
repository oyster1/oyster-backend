package models.objects

import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import models.properties.ProjectList
import models.properties.Named
import models.properties.AccountList
import models.ObjectOrProperty
import models.ObjectType
import models.OysterObject

case class Group(
    uuid: String,
    named: Named,
    accountList: AccountList,
    projectList: ProjectList) extends OysterObject {

  override def asGroup = Some(this)
  override def asNamed = Some(named)
  override def asAccountList = Some(accountList)
  override def asProjectList = Some(projectList)

  def objectType = Group

}

object Group extends ObjectType[Group] {
  val TYPE = "group"

  def createObject(uuid: String, prop: JsValue): Group = {
    lazy val group: Group = Group(uuid, Named.create(group, prop), AccountList.create(group, prop),
      ProjectList.create(group, prop))
    group
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Group] = oysterObject.asGroup
}
