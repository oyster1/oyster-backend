package models.objects

import models.ObjectOrProperty
import models.ObjectType
import models.OysterObject
import models.properties.AccountList
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue

case class Invite(
    uuid: String,
    accountList: AccountList) extends OysterObject {

  override def asInvite = Some(this)
  override def asAccountList = Some(accountList)

  def objectType = Invite

}

object Invite extends ObjectType[Invite] {
  val TYPE = "invite"

  def createObject(uuid: String, prop: JsValue): Invite = {
    lazy val invite: Invite = new Invite(uuid, AccountList.create(invite, prop))
    invite
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Invite] = oysterObject.asInvite
}
