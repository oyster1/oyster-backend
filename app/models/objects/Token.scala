package models.objects

import models.ObjectOrProperty
import models.ObjectType
import models.OysterObject
import models.properties.Authenticates
import models.properties.Deadline
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue

case class Token(
    uuid: String,
    authenticates: Authenticates,
    deadline: Deadline) extends OysterObject {

  override def asToken = Some(this)
  override def asAuthenticates = Some(authenticates)
  override def asDeadline = Some(deadline)

  def objectType = Token

}

object Token extends ObjectType[Token] {
  val TYPE = "token"

  def createObject(uuid: String, prop: JsValue): Token = {
    lazy val token: Token = new Token(uuid, Authenticates.create(token, prop),
      Deadline.create(token, prop))
    token
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Token] = oysterObject.asToken
}
