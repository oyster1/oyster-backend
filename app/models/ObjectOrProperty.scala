package models

import models.objects.Account
import models.objects.Attachment
import models.objects.Comment
import models.objects.Group
import models.objects.Invite
import models.objects.Notification
import models.objects.Project
import models.objects.Task
import models.objects.Token
import models.objects.User
import models.properties.AccountList
import models.properties.Approvable
import models.properties.Archivable
import models.properties.Authenticates
import models.properties.Authorable
import models.properties.Brief
import models.properties.Commentable
import models.properties.Completable
import models.properties.Creatable
import models.properties.Deadline
import models.properties.FullyNamed
import models.properties.LoginCredential
import models.properties.Named
import models.properties.ObjectList
import models.properties.Ownable
import models.properties.Owner
import models.properties.ProjectList
import models.properties.TaskList
import models.properties.Url
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.libs.json.JsString

abstract class ObjectOrProperty {

  def uuid: String
  def objectType: ObjectType[OysterObject]

  def asAccount: Option[Account]
  def asAttachment: Option[Attachment]
  def asComment: Option[Comment]
  def asGroup: Option[Group]
  def asInvite: Option[Invite]
  def asNotification: Option[Notification]
  def asProject: Option[Project]
  def asTask: Option[Task]
  def asToken: Option[Token]
  def asUser: Option[User]

  def asAccountList: Option[AccountList]
  def asApprovable: Option[Approvable]
  def asArchivable: Option[Archivable]
  def asAuthenticates: Option[Authenticates]
  def asAuthorable: Option[Authorable]
  def asBrief: Option[Brief]
  def asCommentable: Option[Commentable]
  def asCompletable: Option[Completable]
  def asCreatable: Option[Creatable]
  def asDeadline: Option[Deadline]
  def asFullyNamed: Option[FullyNamed]
  def asLoginCredential: Option[LoginCredential]
  def asNamed: Option[Named]
  def asObjectList: Option[ObjectList]
  def asOwnable: Option[Ownable]
  def asOwner: Option[Owner]
  def asProjectList: Option[ProjectList]
  def asTaskList: Option[TaskList]
  def asUrl: Option[Url]

  def as[T <: ObjectOrProperty](objectType: ObjectOrPropertyType[T]): Option[T] = {
    objectType.asThisType(this)
  }

  def allProperties: Vector[Property] = PropertyType.ALL.flatMap(as[Property])

  def toDatabaseProperties: JsObject = {
    allProperties.map(_.toDatabaseJson).fold(JsObject(Seq()))(_ ++ _)
  }

  def asDatabaseObject: DatabaseObject[OysterObject] = {
    DatabaseObject(uuid, objectType, toDatabaseProperties)
  }

  def toJsonType: JsObject = JsObject(Seq("type" -> JsString(objectType.TYPE)))

  def toJsonUuid: JsObject = JsObject(Seq("uuid" -> JsString(uuid)))

  def toJsonProperties(implicit db: Database): JsObject = {
    allProperties.map(_.toClientJson).fold(JsObject(Seq()))(_ ++ _)
  }

  def toJsonActions(user: User)(implicit db: Database): JsObject = {
    val modifiableProperties = allProperties.filter(_.canDoAction(user))
    val actionNames = modifiableProperties.flatMap(_.getActionNames)
    JsObject(Seq("actions" -> JsArray(actionNames.map(JsString))))
  }

  def toJsonTypeToUuid: JsObject = JsObject(Seq(objectType.TYPE -> JsString(uuid)))

  def toJsonUuidProperties(implicit db: Database): JsObject = {
    toJsonUuid ++ toJsonProperties
  }

  def toJsonUuidPropertiesProjects(implicit db: Database): JsObject = {
    toJsonUuidProperties ++ asTask.map(_.toJsonProjects).getOrElse(JsObject(Seq.empty))
  }

  def toJsonTypeToUuidProperties(implicit db: Database): JsObject = {
    JsObject(Seq(objectType.TYPE -> toJsonUuidProperties))
  }

  def toJsonTypeToUuidPropertiesProjects(implicit db: Database): JsObject = {
    JsObject(Seq(objectType.TYPE -> toJsonUuidPropertiesProjects))
  }

  def toJsonTypeUuidProperties(implicit db: Database): JsObject = {
    toJsonType ++ toJsonUuidProperties
  }

  def toJsonUuidPropertiesActions(user: User)(implicit db: Database): JsObject = {
    toJsonUuidProperties ++ toJsonActions(user)
  }

}

object ObjectOrProperty extends ObjectOrPropertyType[ObjectOrProperty] {
  def TYPE: String = "object"
  def asThisType(oysterObject: ObjectOrProperty): Option[ObjectOrProperty] = Some(oysterObject)
}
