package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.Property
import models.PropertyType
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Brief(parent: => OysterObject, private var brief: Option[String]) extends Property(parent) {

  def getBrief: String = brief.getOrElse("")

  def setBrief(newBrief: String) {
    brief = Some(newBrief)
  }

  def toDatabaseJson: JsObject = {
    JsObject(brief.map(b => Map(Brief.BRIEF -> JsString(b))).getOrElse(Map.empty))
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitBrief(this)
  override def canDoAction(user: User)(implicit db: Database): Boolean = parent.canSetBrief(user)
  override def getActionNames: Vector[String] = Vector(Brief.ACTION)
}

object Brief extends PropertyType[Brief] {
  def TYPE = BRIEF
  def ACTION = BRIEF

  val BRIEF: String = "brief"

  def create(parent: => OysterObject, json: JsValue): Brief = {
    new Brief(parent, (json \ BRIEF).asOpt[String])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Brief] = oysterObject.asBrief
}
