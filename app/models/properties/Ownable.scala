package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Ownable(parent: => OysterObject, owners: Option[Vector[String]])
    extends References[Owner](parent, owners.getOrElse(Vector.empty)) {

  def referencesType = Ownable

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitOwnable(this)
  override def canDoAction(user: User)(implicit db: Database): Boolean = parent.canAssign(user)
  override def getActionNames: Vector[String] = Vector(Ownable.ACTION)
}

object Ownable extends ReferencesType[Owner] {
  val PROP_NAME: String = "owners"
  val ACTION: String = "assign"

  def create(parent: => OysterObject, json: JsValue): Ownable = {
    new Ownable(parent, (json \ PROP_NAME).asOpt[Vector[String]])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Ownable] = oysterObject.asOwnable

  def referencedType = Owner
}
