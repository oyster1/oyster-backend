package models.properties

import org.mindrot.jbcrypt.BCrypt

import models.DatabaseObject
import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.Property
import models.PropertyType
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class LoginCredential(parent: => OysterObject, private var email: Option[String], private var password: Option[String]) extends Property(parent) {

  def changeEmail(newEmail: String)(implicit db: Database): Option[LoginCredential] = {
    if (email == Some(newEmail) || LoginCredential.findByEmail(newEmail).isEmpty) {
      email = Some(newEmail)
      Some(this)
    } else {
      None
    }
  }

  def changePassword(newPassword: String) {
    password = Some(BCrypt.hashpw(newPassword, BCrypt.gensalt()))
  }

  def checkPassword(proposedPassword: String): Option[LoginCredential] = {
    password.map(BCrypt.checkpw(proposedPassword, _)).flatMap {
      if (_) Some(this) else None
    }
  }

  def toDatabaseJson: JsObject = {
    var map = Map.empty[String, JsValue]
    email.foreach { e =>
      map += LoginCredential.EMAIL -> JsString(e)
    }
    password.foreach { p =>
      map += LoginCredential.PASSWORD -> JsString(p)
    }
    JsObject(map)
  }

  override def toClientJson(implicit db: Database): JsObject = {
    toDatabaseJson - LoginCredential.PASSWORD
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitLoginCredential(this)
}

object LoginCredential extends PropertyType[LoginCredential] {
  def TYPE = "logincredential"

  private val EMAIL = "email"
  private val PASSWORD = "password"

  def create(parent: => OysterObject, json: JsValue): LoginCredential = {
    new LoginCredential(parent, (json \ EMAIL).asOpt[String], (json \ PASSWORD).asOpt[String])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[LoginCredential] = {
    oysterObject.asLoginCredential
  }

  // TODO: should return Option[LoginCredetials]
  def findByEmail(email: String)(implicit db: Database): Option[OysterObject] = {
    val dbObjects = DatabaseObject.loadByProp(EMAIL, email)
    if (dbObjects.length > 1) {
      println("ERROR There are more Users with the same email!")
    }
    dbObjects.map(_.asOysterObject).headOption
  }
}
