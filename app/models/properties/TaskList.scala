package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.objects.Task
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class TaskList(parent: => OysterObject, tasks: Option[Vector[String]])
    extends References[Task](parent, tasks.getOrElse(Vector.empty)) {

  def toJsonTasks(taskToJson: Task => JsObject)(implicit db: Database): JsObject = {
    JsObject(Seq(TaskList.PROP_NAME -> JsArray(getReferencedObjects.flatMap(_.asTask).map(taskToJson))))
  }

  def referencesType = TaskList

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitTaskList(this)
  override def canDoAction(user: User)(implicit db: Database): Boolean = parent.canAddTask(user)
  override def getActionNames: Vector[String] = Vector(TaskList.ACTION)
}

object TaskList extends ReferencesType[Task] {
  val PROP_NAME: String = "tasks"
  private val ACTION = "task"

  def create(parent: => OysterObject, json: JsValue): TaskList = {
    new TaskList(parent, (json \ PROP_NAME).asOpt[Vector[String]])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[TaskList] = oysterObject.asTaskList

  def referencedType = Task
}
