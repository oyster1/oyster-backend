package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyType
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class FullyNamed(parent: => OysterObject, name: Option[String], private var surname: Option[String])
    extends Named(parent, name) {

  def getSurname: String = surname.getOrElse("Donkey")

  def setSurname(newSurname: String) {
    surname = Some(newSurname)
  }

  private def toDatabaseJsonSurname: JsObject = {
    JsObject(surname.map(s => Seq(FullyNamed.SURNAME -> JsString(s))).getOrElse(Seq.empty))
  }

  override def toDatabaseJson: JsObject = super.toDatabaseJson ++ toDatabaseJsonSurname
}

object FullyNamed extends PropertyType[FullyNamed] {
  def TYPE = "fullname"

  private val SURNAME = "surname"

  def create(parent: => OysterObject, json: JsValue): FullyNamed = {
    new FullyNamed(parent, (json \ Named.NAME).asOpt[String], (json \ SURNAME).asOpt[String])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[FullyNamed] = oysterObject.asFullyNamed
}
