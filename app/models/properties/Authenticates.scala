package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Authenticates(parent: => OysterObject, owners: Option[Vector[String]])
    extends References[User](parent, owners.getOrElse(Vector.empty)) {

  def getUser(implicit db: Database): Option[User] = {
    getReferencedObjects.flatMap(_.asUser).headOption
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitAuthenticates(this)
  def referencesType = Authenticates
}

object Authenticates extends ReferencesType[User] {
  val PROP_NAME: String = "authenticates"

  def create(parent: => OysterObject, json: JsValue): Authenticates = {
    new Authenticates(parent, (json \ PROP_NAME).asOpt[Vector[String]])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Authenticates] = oysterObject.asAuthenticates

  def referencedType = User
}
