package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.objects.User
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Authorable(parent: => OysterObject, owners: Option[Vector[String]])
    extends References[User](parent, owners.getOrElse(Vector.empty)) {

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitAuthorable(this)
  def referencesType = Authorable
}

object Authorable extends ReferencesType[User] {
  val PROP_NAME: String = "authors"

  def create(parent: => OysterObject, json: JsValue): Authorable = {
    new Authorable(parent, (json \ PROP_NAME).asOpt[Vector[String]])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Authorable] = oysterObject.asAuthorable

  def referencedType = User
}
