package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.Property
import models.PropertyType
import models.PropertyVisitor
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsBoolean
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Archivable(parent: => OysterObject, private var archived: Option[Boolean]) extends Property(parent) {

  def archive() {
    archived = Some(true)
  }

  def restore() {
    archived = None
  }

  def isArchived: Boolean = {
    archived == Some(true)
  }

  def isVisible: Boolean = {
    !isArchived
  }

  def toDatabaseJson: JsObject = {
    JsObject(archived.map(a => Map(Archivable.ARCHIVED -> JsBoolean(a))).getOrElse(Map.empty))
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitArchivable(this)
  override def canDoAction(user: User)(implicit db: Database): Boolean = parent.canArchive(user)
  override def getActionNames: Vector[String] = Vector(Archivable.ACTION)
}

object Archivable extends PropertyType[Archivable] {
  def TYPE = ARCHIVED

  private val ARCHIVED = "archived"
  private val ACTION = "archive"

  def create(parent: => OysterObject, json: JsValue): Archivable = {
    new Archivable(parent, (json \ ARCHIVED).asOpt[Boolean])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Archivable] = oysterObject.asArchivable
}
