package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class ObjectList(parent: => OysterObject, objects: Option[Vector[String]])
    extends References[ObjectOrProperty](parent, objects.getOrElse(Vector.empty)) {

  override def toClientJson(implicit db: Database): JsObject = {
    val objects = getReferencedObjects
    val objectJsons = objects.map(_.toJsonTypeToUuidPropertiesProjects)
    val objectsJson = objectJsons.fold(JsObject(Seq.empty))(_ ++ _)
    JsObject(Seq(ObjectList.PROP_NAME -> objectsJson))
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitObjectList(this)
  def referencesType = ObjectList
}

object ObjectList extends ReferencesType[ObjectOrProperty] {
  val PROP_NAME: String = "objects"

  def create(parent: => OysterObject, json: JsValue): ObjectList = {
    new ObjectList(parent, (json \ PROP_NAME).asOpt[Vector[String]])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[ObjectList] = oysterObject.asObjectList

  def referencedType = ObjectOrProperty
}
