package models.properties

import play.api.libs.json.Json

case class Url(private var url: Option[String]) {
  
  def chnageUrl(newUrl: String) {
    url = Some(newUrl)
  }

}

object Url {
  implicit val urlFormat = Json.format[Url]
}
