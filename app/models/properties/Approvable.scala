package models.properties

import java.time.Clock
import java.time.LocalDateTime

import models.ObjectOrProperty
import models.OysterObject
import models.Property
import models.PropertyType
import models.PropertyVisitor
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Approvable(parent: => OysterObject, private var approved_at: Option[LocalDateTime])
    extends Property(parent) {

  def approve() {
    approved_at = Some(LocalDateTime.now(Clock.systemUTC()))
  }

  def reject() {
    approved_at = None
  }

  def isApproved: Boolean = approved_at.isDefined

  def isRejected: Boolean = approved_at.isEmpty

  def toDatabaseJson: JsObject = {
    val approvedAt = approved_at.map(_.toString).map(JsString)
    JsObject(approvedAt.map(s => Seq(Approvable.APPROVED_AT -> s)).getOrElse(Seq.empty))
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitApprovable(this)
  override def canDoAction(user: User)(implicit db: Database): Boolean = parent.canApprove(user)
  override def getActionNames: Vector[String] = {
    if (isApproved) Vector(Approvable.REJECT_ACTION) else Vector(Approvable.APPROVE_ACTION)
  }
}

object Approvable extends PropertyType[Approvable] {
  def TYPE = APPROVED_AT

  val APPROVE_ACTION: String = "approve"
  val REJECT_ACTION: String = "reject"
  private val APPROVED_AT: String = "approved_at"

  def create(parent: => OysterObject, json: JsValue): Approvable = {
    new Approvable(parent, (json \ APPROVED_AT).asOpt[LocalDateTime])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Approvable] = oysterObject.asApprovable
}
