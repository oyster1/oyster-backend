package models.properties

import java.time.Clock
import java.time.LocalDateTime

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.Property
import models.PropertyType
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Creatable(parent: => OysterObject, created_at: Option[LocalDateTime])
    extends Property(parent) {

  private val createdAt = created_at.getOrElse(LocalDateTime.now(Clock.systemUTC()))

  def getCreatedAt: LocalDateTime = createdAt

  def toDatabaseJson: JsObject = {
    JsObject(Seq(Creatable.CREATED_AT -> JsString(createdAt.toString)))
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitCreatable(this)
}

object Creatable extends PropertyType[Creatable] {
  def TYPE = CREATED_AT

  private val CREATED_AT = "created_at"

  def create(parent: => OysterObject, json: JsValue): Creatable = {
    new Creatable(parent, (json \ CREATED_AT).asOpt[LocalDateTime])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Creatable] = oysterObject.asCreatable
}
