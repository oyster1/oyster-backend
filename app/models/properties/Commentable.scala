package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.objects.Comment
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Commentable(parent: => OysterObject, comments: Option[Vector[String]])
    extends References[Comment](parent, comments.getOrElse(Vector.empty)) {

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitCommentable(this)
  def referencesType = Commentable
}

object Commentable extends ReferencesType[Comment] {
  val PROP_NAME: String = "comments"

  def create(parent: => OysterObject, json: JsValue): Commentable = {
    new Commentable(parent, (json \ PROP_NAME).asOpt[Vector[String]])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Commentable] = oysterObject.asCommentable

  def referencedType = Comment
}
