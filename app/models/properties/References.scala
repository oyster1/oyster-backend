package models.properties

import models.DatabaseObject
import models.ObjectOrProperty
import models.ObjectOrPropertyType
import models.OysterObject
import models.Property
import models.PropertyType
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue

// TODO: it would be nice to make uuids private, or at least protected
abstract class References[T <: ObjectOrProperty](parent: => OysterObject, var uuids: Vector[String])
    extends Property(parent) {

  def addFirst(o: T) {
    remove(o)
    uuids = o.uuid +: uuids
  }

  def addLast(o: T) {
    remove(o)
    uuids = uuids :+ o.uuid
  }

  def remove(o: T) {
    uuids = uuids.filterNot(_ == o.uuid)
  }

  def contains(o: T): Boolean = uuids.contains(o.uuid)

  // TODO: it would be nicer not to expose uuids, but rather offer type-checked contains method
  def getUuids: Vector[String] = {
    uuids
  }

  def getReferencedObjects(implicit db: Database): Vector[T] = {
    uuids.flatMap(DatabaseObject.load).map(_.asOysterObject).flatMap(_.as(referencedType))
  }

  def referencesType: ReferencesType[T]

  def referencedType: ObjectOrPropertyType[T] = referencesType.referencedType

  def toDatabaseJson: JsObject = createJson(uuids, JsString.apply)

  override def toClientJson(implicit db:Database): JsObject = {
    createJson(uuids, { u: String => JsObject(Seq("uuid" -> JsString(u))) })
  }

  def toJson(tToJson: T => JsObject)(implicit db: Database): JsObject = {
    createJson(getReferencedObjects, tToJson)
  }

  private def createJson[R](list: Seq[R], rToJson: R => JsValue): JsObject = {
    var map = Map.empty[String, JsValue]
    if (list.nonEmpty) {
      map += referencesType.PROP_NAME -> JsArray(list.map(rToJson))
    }
    JsObject(map)
  }
}

abstract class ReferencesType[T <: ObjectOrProperty] extends PropertyType[References[T]] {
  def TYPE = PROP_NAME
  def PROP_NAME: String

  // TODO: add a option to load only objects of a given type
  def getReferiesFor(referencedObject: T)(implicit db: Database): Vector[OysterObject] = {
    val dbObjects = DatabaseObject.loadReferies(PROP_NAME, referencedObject)
    dbObjects.map(_.asOysterObject)
  }

  def referencedType: ObjectOrPropertyType[T]
}
