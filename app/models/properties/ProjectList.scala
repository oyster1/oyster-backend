package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.objects.Project
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class ProjectList(parent: => OysterObject, tasks: Option[Vector[String]])
    extends References[Project](parent, tasks.getOrElse(Vector.empty)) {

  def toJsonTasks(taskToProject: Project => JsObject)(implicit db: Database): JsObject = {
    JsObject(Seq(TaskList.PROP_NAME -> JsArray(getReferencedObjects.flatMap(_.asProject).map(taskToProject))))
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitProjectList(this)
  def referencesType = ProjectList
}

object ProjectList extends ReferencesType[Project] {
  val PROP_NAME: String = "projects"

  def create(parent: => OysterObject, json: JsValue): ProjectList = {
    new ProjectList(parent, (json \ PROP_NAME).asOpt[Vector[String]])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[ProjectList] = oysterObject.asProjectList

  def referencedType = Project
}
