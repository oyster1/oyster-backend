package models.properties

import java.time.LocalDate

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.Property
import models.PropertyType
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Deadline(parent: => OysterObject, private var deadline: Option[LocalDate]) extends Property(parent) {

  def setDeadline(newDeadline: LocalDate) {
    deadline = Some(newDeadline)
  }

  def removeDeadline() {
    deadline = None
  }

  def toDatabaseJson: JsObject = {
    JsObject(deadline.map(b => Map(Deadline.DEADLINE -> JsString(b.toString))).getOrElse(Map.empty))
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitDeadline(this)
  override def canDoAction(user: User)(implicit db: Database): Boolean = parent.canChangeDeadline(user)
  override def getActionNames: Vector[String] = Vector(Deadline.ACTION)
}

object Deadline extends PropertyType[Deadline] {
  def TYPE = DEADLINE
  def ACTION = DEADLINE

  val DEADLINE = "deadline"

  def create(parent: => OysterObject, json: JsValue): Deadline = {
    new Deadline(parent, (json \ DEADLINE).asOpt[LocalDate])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Deadline] = oysterObject.asDeadline
}
