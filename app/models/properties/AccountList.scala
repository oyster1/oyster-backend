package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.objects.Account
import play.api.db.Database
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class AccountList(parent: => OysterObject, accounts: Option[Vector[String]])
    extends References[Account](parent, accounts.getOrElse(Vector.empty)) {

  def getAccount(implicit db: Database): Option[Account] = {
    getReferencedObjects.flatMap(_.asAccount).headOption
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitAccountList(this)
  def referencesType = AccountList
}

object AccountList extends ReferencesType[Account] {
  val PROP_NAME: String = "accounts"

  def create(parent: => OysterObject, json: JsValue): AccountList = {
    new AccountList(parent, (json \ PROP_NAME).asOpt[Vector[String]])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[AccountList] = oysterObject.asAccountList

  def referencedType = Account
}
