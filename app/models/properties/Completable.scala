package models.properties

import java.time.Clock
import java.time.LocalDateTime

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.Property
import models.PropertyType
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Completable(parent: => OysterObject, private var completed_at: Option[LocalDateTime]) extends Property(parent) {

  def complete() {
    completed_at = Some(LocalDateTime.now(Clock.systemUTC()))
  }

  def reopen() {
    completed_at = None
  }

  def isCompleted: Boolean = completed_at.isDefined

  def isOpen: Boolean = completed_at.isEmpty

  def toDatabaseJson: JsObject = {
    JsObject(completed_at.map(c => Map(Completable.COMPLETED_AT -> JsString(c.toString))).getOrElse(Map.empty))
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitCompletable(this)
  override def canDoAction(user: User)(implicit db: Database): Boolean = parent.canComplete(user)
  override def getActionNames: Vector[String] = {
    if (isCompleted) Vector(Completable.REOPEN_ACTION) else Vector(Completable.COMPLETE_ACTION)
  }
}

object Completable extends PropertyType[Completable] {
  def TYPE = COMPLETED_AT

  val COMPLETE_ACTION: String = "complete"
  val REOPEN_ACTION: String = "reopen"
  private val COMPLETED_AT = "completed_at"

  def create(parent: => OysterObject, json: JsValue): Completable = {
    new Completable(parent, (json \ COMPLETED_AT).asOpt[LocalDateTime])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Completable] = oysterObject.asCompletable
}
