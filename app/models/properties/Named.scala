package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.PropertyVisitor
import models.Property
import models.PropertyType
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue.jsValueToJsLookup

class Named(parent: => OysterObject, private var name: Option[String]) extends Property(parent) {

  def getName: String = {
    name.getOrElse("unnamed")
  }

  def rename(newName: String) {
    name = Some(newName)
  }

  def toDatabaseJson: JsObject = {
    JsObject(name.map(n => Map(Named.NAME -> JsString(n))).getOrElse(Map.empty))
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitNamed(this)
  override def canDoAction(user: User)(implicit db: Database): Boolean = parent.canRename(user)
  override def getActionNames: Vector[String] = Vector(Named.ACTION)
}

object Named extends PropertyType[Named] {
  def TYPE = NAME

  val ACTION = "rename"
  private[properties] val NAME = "name"

  def create(parent: => OysterObject, json: JsValue): Named = {
    new Named(parent, (json \ NAME).asOpt[String])
  }

  def asThisType(oysterObject: ObjectOrProperty): Option[Named] = oysterObject.asNamed
}
