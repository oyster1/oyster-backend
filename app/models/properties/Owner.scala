package models.properties

import models.ObjectOrProperty
import models.OysterObject
import models.Property
import models.PropertyType
import models.PropertyVisitor
import models.objects.Project
import models.objects.Task
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue

class Owner(parent: => OysterObject) extends Property(parent) {
  def toDatabaseJson: JsObject = JsObject(Map.empty[String, JsObject])

  def getAssignedObjects(implicit db: Database): Vector[OysterObject] = {
    Ownable.getReferiesFor(this)
  }

  def getAssignedTasks(implicit db: Database): Vector[Task] = getAssignedObjects.flatMap(_.asTask)

  def getAssignedProjects(implicit db: Database): Vector[Project] = {
    getAssignedObjects.flatMap(_.asProject)
  }

  def accept[T](visitor: PropertyVisitor[T]): T = visitor.visitOwner(this)
}

object Owner extends PropertyType[Owner] {
  def TYPE = "owner"

  def create(parent: => OysterObject, json: JsValue): Owner = new Owner(parent)

  def asThisType(oysterObject: ObjectOrProperty): Option[Owner] = oysterObject.asOwner
}
