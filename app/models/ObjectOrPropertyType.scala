package models

abstract class ObjectOrPropertyType[+T <: ObjectOrProperty] {
  def TYPE: String
  def asThisType(oysterObject: ObjectOrProperty): Option[T]
}
