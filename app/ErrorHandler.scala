import javax.inject._

import play.api.http.DefaultHttpErrorHandler
import play.api._
import play.api.mvc._
import play.api.mvc.Results._
import play.api.routing.Router
import scala.concurrent._
import play.api.libs.json.JsValue
import play.api.libs.json.Json

import controllers.Errors._

class ErrorHandler @Inject() (
    env: Environment,
    config: Configuration,
    sourceMapper: OptionalSourceMapper,
    router: Provider[Router]) extends DefaultHttpErrorHandler(env, config, sourceMapper, router) {

  override def onBadRequest(request: RequestHeader, message: String): Future[Result] = {
    Future.successful(GenericBadRequest(message))
  }

  override def onForbidden(request: RequestHeader, message: String): Future[Result] = {
    Future.successful(GenericForbidden(message))
  }

  override def onNotFound(request: RequestHeader, message: String): Future[Result] = {
    Future.successful(GenericNotFound(message))
  }

  override def onOtherClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] = {
    Future.successful(GenericClientError(message))
  }

  override def onProdServerError(request: RequestHeader, exception: UsefulException): Future[Result] = {
    Future.successful(GenericServerError(exception.getMessage))
  }

  override def onDevServerError(request: RequestHeader, exception: UsefulException): Future[Result] = {
    onProdServerError(request, exception)
  }
}
