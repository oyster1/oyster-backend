package views

import models.ObjectOrProperty
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue

abstract class OysterView[-T <: ObjectOrProperty] {

  def apply[R <: T](objectOrProperty: R)(implicit user: User, db: Database): JsObject = {
    JsObject(getAttributes(objectOrProperty))
  }

  def getAttributes(objectOrProperty: T)(
    implicit user: User, db: Database): Seq[(String, JsValue)]

  def +[S <: T](second: OysterView[S]): OysterView[S] = {
    val first = this
    new OysterView[S] {
      def getAttributes(objectOrProperty: S)(implicit user: User, db: Database) = {
        first.getAttributes(objectOrProperty) ++ second.getAttributes(objectOrProperty)
      }
    }
  }

}