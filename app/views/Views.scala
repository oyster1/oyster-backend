package views

object Views {
  val UuidPropertiesView = UuidView + PropertiesView
  val UuidPropertiesActionsView = UuidView + PropertiesView + ActionsView
}
