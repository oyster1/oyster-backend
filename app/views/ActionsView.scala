package views

import models.ObjectOrProperty
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsArray
import play.api.libs.json.JsString
import play.api.libs.json.JsValue

object ActionsView extends OysterView[ObjectOrProperty] {

  private val ACTIONS: String = "actions"

  def getAttributes(objectOrProperty: ObjectOrProperty)(
    implicit user: User, db: Database): Seq[(String, JsValue)] = {
    val modifiableProperties = objectOrProperty.allProperties.filter(_.canDoAction(user))
    val actionNames = modifiableProperties.flatMap(_.getActionNames)
    Seq(ACTIONS -> JsArray(actionNames.map(JsString)))
  }

}
