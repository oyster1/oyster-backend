package views

import models.ObjectOrProperty
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsArray

class ArrayView[-T <: ObjectOrProperty](objectView: OysterView[T]) {

  def apply[R <: T](objects: Seq[R])(implicit user: User, db: Database): JsArray = {
    JsArray(objects.map(objectView.apply))
  }

}
