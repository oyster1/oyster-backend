package views

import models.ObjectOrProperty
import models.objects.User
import play.api.db.Database
import play.api.libs.json.JsString
import play.api.libs.json.JsValue

object TypeToUuidView extends OysterView[ObjectOrProperty] {

  def getAttributes(objectOrProperty: ObjectOrProperty)(
    implicit user: User, db: Database): Seq[(String, JsValue)] = {
    Seq(objectOrProperty.objectType.TYPE -> JsString(objectOrProperty.uuid))
  }

}
