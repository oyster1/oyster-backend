package views

import models.ObjectOrProperty
import play.api.db.Database
import play.api.libs.json.JsObject
import play.api.libs.json.JsValue
import models.objects.User

object PropertiesView extends OysterView[ObjectOrProperty] {

  def getAttributes(objectOrProperty: ObjectOrProperty)(
    implicit user: User, db: Database): Seq[(String, JsValue)] = {
    val properties = objectOrProperty.allProperties.map(_.toClientJson)
    properties.foldLeft[Seq[(String, JsValue)]](Seq())(_ ++ _.fields)
  }

}
