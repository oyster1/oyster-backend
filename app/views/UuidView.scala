package views

import models.ObjectOrProperty
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.db.Database
import models.objects.User

object UuidView extends OysterView[ObjectOrProperty] {

  private val UUID: String = "uuid"

  def getAttributes(objectOrProperty: ObjectOrProperty)(
    implicit user: User, db: Database): Seq[(String, JsValue)] = {
    Seq(UUID -> JsString(objectOrProperty.uuid))
  }

}
