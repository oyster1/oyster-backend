package controllers

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import org.junit.Before
import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.Database
import play.api.db.evolutions.SimpleEvolutionsReader
import play.api.db.evolutions.Evolution
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import play.api.libs.json.JsValue

class InviteSpecs extends PlaySpec with OneAppPerSuite with BeforeAndAfterAll {

  // Clean up test user before running tests
  override def beforeAll {
    val db = app.injector.instanceOf[Database]
    db.withConnection { conn =>
      conn.prepareStatement("DELETE FROM Object AS T USING Object as U WHERE T.type = 'token'" +
        " AND U.type = 'user' AND T.prop->'authenticates' ?? U.uuid" +
        " AND (U.prop->'email' ?? 'test@getoyster.com'" +
        " OR U.prop->'email' ?? 'test2@getoyster.com')").executeUpdate()
      conn.prepareStatement("DELETE FROM Object WHERE type = 'user'" +
        " AND (prop->'email' ?? 'test@getoyster.com'" +
        " OR prop->'email' ?? 'test2@getoyster.com')").executeUpdate()
    }
  }

  var invite = ""
  var invite2 = ""
  var userUuid = ""
  var user2Uuid = ""
  var token = ""
  var token2 = ""

  "Invite" should {

    "create a new user account" in {
      val request = FakeRequest(POST, "/account")
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name":"Testing Account"}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "invite").as[String] must not be empty

      invite = (contentAsJson(response) \ "invite").as[String]
    }

    "sing up with an invite" in {
      val request = FakeRequest(POST, "/user/sign-up")
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"email":"test@getoyster.com","password":"1234","name":"Franta",""" +
          """"surname":"Oprsalek","invite":"""" + invite + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty
      (contentAsJson(response) \ "token").as[String] must not be empty

      userUuid = (contentAsJson(response) \ "uuid").as[String]
      token = (contentAsJson(response) \ "token").as[String]
    }

    "check account name" in {
      val request = FakeRequest(GET, "/account/mine")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Account"
    }

    "verify user is member of the account" in {
      val request = FakeRequest(GET, "/account/users")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response)).as[List[JsValue]].map(_ \ "uuid").map(_.as[String]) mustEqual List(userUuid)
    }

    "invite another user to join the account" in {
      val request = FakeRequest(POST, "/account/invite")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "invite").as[String] must not be empty

      invite2 = (contentAsJson(response) \ "invite").as[String]
    }

    "sing up with user generated invite" in {
      val request = FakeRequest(POST, "/user/sign-up")
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"email":"test2@getoyster.com","password":"12345","name":"Pepa","""
          + """"surname":"Omacka","invite":"""" + invite2 + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty
      (contentAsJson(response) \ "token").as[String] must not be empty

      user2Uuid = (contentAsJson(response) \ "uuid").as[String]
      token2 = (contentAsJson(response) \ "token").as[String]
    }

    "check account name by second user" in {
      val request = FakeRequest(GET, "/account/mine")
        .withHeaders("Authorization-Token" -> token2)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Account"
    }

    "verify both users are members of the account" in {
      val request = FakeRequest(GET, "/account/users")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val userUuids = (contentAsJson(response)).as[List[JsValue]].map(_ \ "uuid").map(_.as[String])
      userUuids must (contain(userUuid) and contain(user2Uuid) and have size 2)
    }
  }
}
