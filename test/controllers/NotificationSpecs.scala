package controllers

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import org.junit.Before
import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.Database
import play.api.db.evolutions.SimpleEvolutionsReader
import play.api.db.evolutions.Evolution
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import models.DatabaseObject
import models.objects.User
import models.objects.Token
import models.objects.Project
import play.api.libs.json.JsValue
import java.time.LocalDateTime
import java.time.Clock
import java.time.Duration
import models.objects.Task

class NotificationSpecs extends PlaySpec with OneAppPerSuite with BeforeAndAfterAll {

  var tokenA = ""
  var tokenB = ""
  var userAUuid = ""
  var userBUuid = ""
  var projectUuid = ""
  var taskUuid = ""
  var notificationUuid = ""
  var notificationActions = List.empty[String]

  // Create testing objects
  override def beforeAll {
    implicit val db = app.injector.instanceOf[Database]
    val userA = DatabaseObject.create(User).asOysterObject
    val userB = DatabaseObject.create(User).asOysterObject
    val tokenA = DatabaseObject.create(Token).asOysterObject
    val tokenB = DatabaseObject.create(Token).asOysterObject
    val project = DatabaseObject.create(Project).asOysterObject
    val task = DatabaseObject.create(Task).asOysterObject
    this.tokenA = tokenA.uuid
    this.tokenB = tokenB.uuid
    userAUuid = userA.uuid
    userBUuid = userB.uuid
    projectUuid = project.uuid
    taskUuid = task.uuid
    userA.fullyNamed.rename("User A")
    userB.fullyNamed.rename("user B")
    tokenA.authenticates.addLast(userA)
    tokenB.authenticates.addLast(userB)
    project.ownable.addLast(userA.owner)
    project.ownable.addLast(userB.owner)
    project.taskList.addLast(task)
    task.ownable.addLast(userA.owner)
    userA.asDatabaseObject.save()
    userB.asDatabaseObject.save()
    tokenA.asDatabaseObject.save()
    tokenB.asDatabaseObject.save()
    project.asDatabaseObject.save()
    task.asDatabaseObject.save()
  }

  "Notifications" should {

    "generate notification from rename project action" in {
      val request = FakeRequest(PUT, "/project/" + projectUuid + "/rename")
        .withHeaders("Authorization-Token" -> tokenA)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name": "Renamed By User A"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Renamed By User A"
    }

    "not give notification to user initiating the action" in {
      val request = FakeRequest(GET, "/notifications")
        .withHeaders("Authorization-Token" -> tokenA)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")

      val notifications = (contentAsJson(response)).as[List[JsValue]]
      notifications.size mustEqual 0
    }

    "give notification to project owner" in {
      val request = FakeRequest(GET, "/notifications")
        .withHeaders("Authorization-Token" -> tokenB)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")

      val notifications = (contentAsJson(response)).as[List[JsValue]]
      notifications.size mustEqual 1
      val projectNotification = notifications(0)

      notificationUuid = (projectNotification \ "uuid").as[String]
      notificationUuid must not be empty

      val owners = (projectNotification \ "owners").as[List[JsValue]]
      owners.map(_ \ "uuid").map(_.as[String]) must contain(userBUuid)

      val created = (projectNotification \ "created_at").as[LocalDateTime]
      val timeDifference = Duration.between(created, LocalDateTime.now(Clock.systemUTC)).abs.toMillis
      // check created happened "now" with 1000 miliseconds tolerance
      timeDifference must be < 1000L

      val authors = (projectNotification \ "authors").as[List[JsValue]]
      authors.size mustEqual 1
      val userAAuthor = authors(0)
      (userAAuthor \ "uuid").as[String] mustEqual userAUuid
      (userAAuthor \ "name").as[String] mustEqual "User A"

      (projectNotification \ "name").as[String] mustEqual "rename"

      val projectObject = (projectNotification \ "objects" \ "project")
      (projectObject \ "uuid").as[String] mustEqual projectUuid
      (projectObject \ "name").as[String] mustEqual "Renamed By User A"

      (projectNotification \ "completed_at").asOpt[LocalDateTime] mustEqual None

      notificationActions = (projectNotification \ "actions").as[List[String]]
    }

    "set notification as completed" in {
      notificationActions must contain("complete")

      val request = FakeRequest(PUT, "/notification/" + notificationUuid + "/complete")
        .withHeaders("Authorization-Token" -> tokenB)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val completed = (contentAsJson(response) \ "completed_at").as[LocalDateTime]
      val timeDifference = Duration.between(completed, LocalDateTime.now(Clock.systemUTC)).abs.toMillis
      // check completed happened "now" with 100 miliseconds tolerance
      timeDifference must be < 100L
    }

    "show notification as completed" in {
      val request = FakeRequest(GET, "/notifications")
        .withHeaders("Authorization-Token" -> tokenB)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")

      val notifications = (contentAsJson(response)).as[List[JsValue]]
      notifications.size mustEqual 1
      val projectNotification = notifications(0)

      val completed = (projectNotification \ "completed_at").as[LocalDateTime]
      val timeDifference = Duration.between(completed, LocalDateTime.now(Clock.systemUTC)).abs.toMillis
      // check completed happened "now" with 1 second tolerance
      timeDifference must be < 1000L
    }

    "generate notification from rename task action" in {
      val request = FakeRequest(PUT, "/task/" + taskUuid + "/rename")
        .withHeaders("Authorization-Token" -> tokenB)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name": "Named By User B"}""")
      val response = route(app, request).get
      status(response) mustBe 200
    }

    "generate notification from set brief task action" in {
      val request = FakeRequest(PUT, "/task/" + taskUuid + "/brief")
        .withHeaders("Authorization-Token" -> tokenB)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"brief": "Briefd By User B"}""")
      val response = route(app, request).get
      status(response) mustBe 200
    }

    "generate notification from change deadline task action" in {
      val request = FakeRequest(PUT, "/task/" + taskUuid + "/deadline")
        .withHeaders("Authorization-Token" -> tokenB)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"deadline": "2017-05-30"}""")
      val response = route(app, request).get
      status(response) mustBe 200
    }

    "check UserA name, brief, deadline notifications" in {
      val request = FakeRequest(GET, "/notifications")
        .withHeaders("Authorization-Token" -> tokenA)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val notifications = (contentAsJson(response)).as[List[JsValue]]

      val deadlineNotification = notifications(0)
      (deadlineNotification \ "name").as[String] mustEqual "deadline"
      (deadlineNotification \ "objects" \ "task" \ "uuid").as[String] mustEqual taskUuid
      val dProjects = (deadlineNotification \ "objects" \ "task" \ "projects").as[List[JsValue]]
      dProjects.map(_ \ "uuid").map(_.as[String]) must contain(projectUuid)

      val briefNotification = notifications(1)
      (briefNotification \ "name").as[String] mustEqual "brief"
      (briefNotification \ "objects" \ "task" \ "uuid").as[String] mustEqual taskUuid
      val bProjects = (deadlineNotification \ "objects" \ "task" \ "projects").as[List[JsValue]]
      bProjects.map(_ \ "uuid").map(_.as[String]) must contain(projectUuid)

      val renamedNotification = notifications(2)
      (renamedNotification \ "name").as[String] mustEqual "rename"
      (renamedNotification \ "objects" \ "task" \ "uuid").as[String] mustEqual taskUuid
      val rProjects = (deadlineNotification \ "objects" \ "task" \ "projects").as[List[JsValue]]
      rProjects.map(_ \ "uuid").map(_.as[String]) must contain(projectUuid)

    }

    "complete the task" in {
      val request = FakeRequest(PUT, "/task/" + taskUuid + "/complete")
        .withHeaders("Authorization-Token" -> tokenA)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get
      status(response) mustBe 200
    }

    "generate notification from aprrove task action" in {
      val request = FakeRequest(PUT, "/task/" + taskUuid + "/approve")
        .withHeaders("Authorization-Token" -> tokenB)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get
      status(response) mustBe 200
    }

    "check UserB approve, reopen notifications" in {
      val request = FakeRequest(GET, "/notifications")
        .withHeaders("Authorization-Token" -> tokenA)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val notifications = (contentAsJson(response)).as[List[JsValue]]

      val approveNotification = notifications(0)
      (approveNotification \ "name").as[String] mustEqual "approve"
      (approveNotification \ "objects" \ "task" \ "uuid").as[String] mustEqual taskUuid
    }

  }

}
