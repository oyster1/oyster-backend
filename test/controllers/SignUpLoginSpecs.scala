package controllers

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import org.junit.Before
import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.Database
import play.api.db.evolutions.SimpleEvolutionsReader
import play.api.db.evolutions.Evolution
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import models.DatabaseObject
import models.objects.Account
import models.objects.Invite

class SignUpLoginSpecs extends PlaySpec with OneAppPerSuite with BeforeAndAfterAll {

  // Clean up test user before running tests
  override def beforeAll {
    implicit val db = app.injector.instanceOf[Database]
    db.withConnection { conn =>
      conn.prepareStatement("DELETE FROM Object AS T USING Object as U WHERE T.type = 'token' AND U.type = 'user' AND T.prop->'authenticates' ?? U.uuid").executeUpdate()
      conn.prepareStatement("DELETE FROM Object WHERE prop->'email' ?? 'pepik@test.com'").executeUpdate()
    }
    val account = DatabaseObject.create(Account).asOysterObject
    account.named.rename("Testing Account")
    val invite = DatabaseObject.create(Invite).asOysterObject
    invite.accountList.addLast(account)
    account.asDatabaseObject.save()
    invite.asDatabaseObject.save()
    this.invite = invite.uuid
  }

  var invite = ""
  var userUuid = ""
  var signUpToken = ""
  var loginToken = ""

  "User Controller" should {

    "allow user sign up" in {
      val request = FakeRequest(POST, "/user/sign-up")
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"email":"pepik@test.com","password":"1234","name":"Pepik","""
          + """"surname":"Sedlacek","invite":"""" + invite + """"}""")
      val response = route(app, request).get

      status(response) mustBe (200)
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty
      (contentAsJson(response) \ "token").as[String] must not be empty

      userUuid = (contentAsJson(response) \ "uuid").as[String]
      signUpToken = (contentAsJson(response) \ "token").as[String]
    }

    "verify sing up token is valid" in {
      val request = FakeRequest(GET, "/user/me")
        .withHeaders("Authorization-Token" -> signUpToken)
      val response = route(app, request).get

      status(response) mustBe (200)
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] mustEqual userUuid
      (contentAsJson(response) \ "name").as[String] mustEqual "Pepik"
      (contentAsJson(response) \ "surname").as[String] mustEqual "Sedlacek"
    }

    "allow user login" in {
      val request = FakeRequest(POST, "/user/login")
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"email":"pepik@test.com","password":"1234"}""")
      val response = route(app, request).get

      status(response) mustBe (200)
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] mustEqual userUuid
      (contentAsJson(response) \ "token").as[String] must not be empty

      loginToken = (contentAsJson(response) \ "token").as[String]
    }

    "verify login token is valid" in {
      val request = FakeRequest(GET, "/user/me")
        .withHeaders("Authorization-Token" -> loginToken)
      val response = route(app, request).get

      status(response) mustBe (200)
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] mustEqual userUuid
      (contentAsJson(response) \ "name").as[String] mustEqual "Pepik"
      (contentAsJson(response) \ "surname").as[String] mustEqual "Sedlacek"
    }

    "not allow the same email to be used twice" in {
      val request = FakeRequest(POST, "/user/sign-up")
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"email":"pepik@test.com","password":"abcd","name":"Pepik2","""
          + """"surname":"Sedlacek2","invite":"""" + invite + """"}""")
      val response = route(app, request).get

      status(response) mustBe (409)
      contentType(response) mustBe Some("application/json")
    }

    "logout with token from sing up" in {
      val request = FakeRequest(POST, "/user/logout")
        .withHeaders("Authorization-Token" -> signUpToken)
      val response = route(app, request).get

      status(response) mustBe 204
    }

    "after logout token is not longer valid" in {
      val request = FakeRequest(GET, "/user/me")
        .withHeaders("Authorization-Token" -> signUpToken)
      val response = route(app, request).get

      status(response) mustBe 401
      contentType(response) mustBe Some("application/json")
      val error = (contentAsJson(response) \ "error").as[String]
      error mustEqual "you need to sign in to access this page"
    }

    "subsequent logouts give error" in {
      val request = FakeRequest(POST, "/user/logout")
        .withHeaders("Authorization-Token" -> signUpToken)
      val response = route(app, request).get

      status(response) mustBe 410
      val error = (contentAsJson(response) \ "error").as[String]
      error mustEqual "this token is already invalid"
    }

    "other tokens are not affected" in {
      val request = FakeRequest(GET, "/user/me")
        .withHeaders("Authorization-Token" -> loginToken)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] mustEqual userUuid
      (contentAsJson(response) \ "name").as[String] mustEqual "Pepik"
      (contentAsJson(response) \ "surname").as[String] mustEqual "Sedlacek"
    }
  }
}
