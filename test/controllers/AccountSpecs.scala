package controllers

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import org.junit.Before
import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.Database
import play.api.db.evolutions.SimpleEvolutionsReader
import play.api.db.evolutions.Evolution
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import play.api.libs.json.JsValue

class AccountSpecs extends PlaySpec with OneAppPerSuite with BeforeAndAfterAll {

  // Clean up test user before running tests
  override def beforeAll {
    val db = app.injector.instanceOf[Database]
    db.withConnection { conn =>
      conn.prepareStatement("""
        DELETE FROM Object AS T
        USING Object as U
        WHERE T.type = 'token'
        AND U.type = 'user'
        AND U.prop->'email' ?? 'john@smith.com'
        AND T.prop->'authenticates' ?? U.uuid""").executeUpdate()
      conn.prepareStatement("""
        DELETE FROM Object
        WHERE prop->'email' ?? 'john@smith.com'""").executeUpdate()
    }
  }

  var invite = ""
  var userUuid = ""
  var token = ""
  var projectUuid = ""
  var taskUuid = ""
  var groupUuid = ""

  "Account" should {

    "create a new user account" in {
      val request = FakeRequest(POST, "/account")
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name":"Testing Account"}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "invite").as[String] must not be empty

      invite = (contentAsJson(response) \ "invite").as[String]
    }

    "create user in Testing Account" in {
      val request = FakeRequest(POST, "/user/sign-up")
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"email":"john@smith.com","password":"12345","name":"John","surname":"Smith","""
          + "\"invite\":\"" + invite + "\"}")
      val response = route(app, request).get

      status(response) mustBe (200)
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty
      (contentAsJson(response) \ "token").as[String] must not be empty

      userUuid = (contentAsJson(response) \ "uuid").as[String]
      token = (contentAsJson(response) \ "token").as[String]
    }

    "verify user is member of user's account" in {
      val request = FakeRequest(GET, "/account/users")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val users = (contentAsJson(response)).as[List[JsValue]]
      users.map(_ \ "uuid").map(_.as[String]) must contain(userUuid)
    }

    var projectActions = List.empty[String]
    "create project in user's account" in {
      val request = FakeRequest(POST, "/account/project")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name":"Testing Project","brief":"This project is for testing.",""" +
          """"deadline":"2017-05-05"}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty

      projectActions = (contentAsJson(response) \ "actions").as[List[String]]
      projectUuid = (contentAsJson(response) \ "uuid").as[String]
    }

    "new project can be renamed, briefed, deadline" in {
      projectActions must contain("rename")
      projectActions must contain("brief")
      projectActions must contain("deadline")
    }

    "verify project is in of user's account" in {
      val request = FakeRequest(GET, "/account/projects")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")

      val projects = (contentAsJson(response)).as[List[JsValue]]
      val project = projects.find(j => (j \ "uuid").as[String] == projectUuid)
      project must not be None
      (project.get \ "name").as[String] mustEqual "Testing Project"
      (project.get \ "brief").as[String] mustEqual "This project is for testing."
      (project.get \ "deadline").as[String] mustEqual "2017-05-05"
      val owners = (project.get \ "owners").as[List[JsValue]]
      owners.map(_ \ "uuid").map(_.as[String]) must contain(userUuid)
    }

    "create task in project in user's account" in {
      val request = FakeRequest(POST, "/project/" + projectUuid + "/task")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name": "Task Inside a Project"}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty
      (contentAsJson(response) \ "name").as[String] mustEqual "Task Inside a Project"

      taskUuid = (contentAsJson(response) \ "uuid").as[String]
    }

    "verify task is inside a project in a user's account" in {
      val request = FakeRequest(GET, "/account/projects")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val projects = (contentAsJson(response)).as[List[JsValue]]
      val project = projects.find(j => (j \ "uuid").as[String] == projectUuid)
      project must not be None
      val tasks = project.map(_ \ "tasks").get.as[List[JsValue]]
      tasks.size mustEqual 1
      (tasks(0) \ "uuid").as[String] mustEqual taskUuid
      (tasks(0) \ "name").as[String] mustEqual "Task Inside a Project"
      (tasks(0) \ "actions").as[List[String]] must contain("brief")
    }

    "create group in user's account" in {
      val request = FakeRequest(POST, "/group")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty

      groupUuid = (contentAsJson(response) \ "uuid").as[String]
    }

    "verify group is in of user's account" in {
      val request = FakeRequest(GET, "/account/groups")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val groups = (contentAsJson(response)).as[List[JsValue]]
      groups.map((_ \ "uuid")).map(_.as[String]) must contain(groupUuid)
    }

    "archive project" in {
      val request = FakeRequest(DELETE, "/project/" + projectUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "archived").as[Boolean] mustBe true
    }

    "verify archived project is not shown" in {
      val request = FakeRequest(GET, "/account/projects")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val projects = (contentAsJson(response)).as[List[JsValue]]
      projects.map((_ \ "uuid")).map(_.as[String]) must not contain (projectUuid)
    }
  }
}
