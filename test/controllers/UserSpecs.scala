package controllers

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import org.junit.Before
import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.Database
import play.api.db.evolutions.SimpleEvolutionsReader
import play.api.db.evolutions.Evolution
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import models.DatabaseObject
import models.objects.User
import models.objects.Token
import models.objects.Project
import models.objects.Task
import play.api.libs.json.JsValue

class UserSpecs extends PlaySpec with OneAppPerSuite with BeforeAndAfterAll {

  var token = ""
  var userUuid = ""
  var userActions: List[String] = Nil

  // Create testing objects
  override def beforeAll {
    implicit val db = app.injector.instanceOf[Database]
    val user = DatabaseObject.create(User).asOysterObject
    val token = DatabaseObject.create(Token).asOysterObject
    userUuid = user.uuid
    this.token = token.uuid
    token.authenticates.addLast(user)
    user.asDatabaseObject.save()
    token.asDatabaseObject.save()
  }

  "User" should {

    "get user actions" in {
      val request = FakeRequest(GET, "/user/" + userUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] mustEqual userUuid

      userActions = (contentAsJson(response) \ "actions").as[List[String]]
    }

    "change user name and surname" in {
      userActions must contain("rename")

      val request = FakeRequest(PUT, "/user/" + userUuid + "/rename")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name":"Pepik","surname":"Sedlacek"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Pepik"
      (contentAsJson(response) \ "surname").as[String] mustEqual "Sedlacek"
    }

    "user's name is correctly updated" in {
      val request = FakeRequest(GET, "/user/" + userUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] mustEqual userUuid
      (contentAsJson(response) \ "name").as[String] mustEqual "Pepik"
      (contentAsJson(response) \ "surname").as[String] mustEqual "Sedlacek"
    }
  }
}
