package controllers

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import org.junit.Before
import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.Database
import play.api.db.evolutions.SimpleEvolutionsReader
import play.api.db.evolutions.Evolution
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import models.DatabaseObject
import models.objects.User
import models.objects.Token
import models.objects.Project
import play.api.libs.json.JsValue

class GroupSpecs extends PlaySpec with OneAppPerSuite with BeforeAndAfterAll {

  var token = ""
  var groupUuid = ""
  var projectUuid = ""

  // Create testing objects
  override def beforeAll {
    implicit val db = app.injector.instanceOf[Database]
    val user = DatabaseObject.create(User).asOysterObject
    val project = DatabaseObject.create(Project).asOysterObject
    val token = DatabaseObject.create(Token).asOysterObject
    this.token = token.uuid
    projectUuid = project.uuid
    token.authenticates.addLast(user)
    user.asDatabaseObject.save()
    project.asDatabaseObject.save()
    token.asDatabaseObject.save()
  }

  "Group" should {

    "create empty group" in {
      val request = FakeRequest(POST, "/group")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty

      groupUuid = (contentAsJson(response) \ "uuid").as[String]
    }

    "check group is empty" in {
      val request = FakeRequest(GET, "/group/" + groupUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "projects").asOpt[List[String]] mustEqual None
    }

    "rename group" in {
      val request = FakeRequest(PUT, "/group/" + groupUuid + "/rename")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name": "Testing Group"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Group"
    }

    "add project to a group" in {
      val request = FakeRequest(PUT, "/project/" + projectUuid + "/move_to_group")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"group": """" + groupUuid + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
    }

    "view group" in {
      val request = FakeRequest(GET, "/group/" + groupUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Group"
      val projects = (contentAsJson(response) \ "projects").as[List[JsValue]]
      projects.map(_ \ "uuid").map(_.as[String]) must contain(projectUuid)
    }

    "archive project" in {
      val request = FakeRequest(DELETE, "/project/" + projectUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "archived").as[Boolean] mustEqual true
    }

    "archived project is not part of a group" in {
      val request = FakeRequest(GET, "/group/" + groupUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Group"
      val projects = (contentAsJson(response) \ "projects").asOpt[List[String]].getOrElse(Nil)
      projects must not contain(projectUuid)
    }
  }
}
