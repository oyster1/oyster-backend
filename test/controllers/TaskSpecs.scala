package controllers

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import org.junit.Before
import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.Database
import play.api.db.evolutions.SimpleEvolutionsReader
import play.api.db.evolutions.Evolution
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import models.DatabaseObject
import models.objects.User
import models.objects.Token
import models.objects.Project
import java.time.LocalDateTime
import java.time.Clock
import java.time.Duration
import play.api.libs.json.JsValue

class TaskSpecs extends PlaySpec with OneAppPerSuite with BeforeAndAfterAll {

  var token = ""
  var userUuid = ""
  var taskUuid = ""
  var projectUuid = ""
  var taskActions = List.empty[String]

  // Create testing objects
  override def beforeAll {
    implicit val db = app.injector.instanceOf[Database]
    val user = DatabaseObject.create(User).asOysterObject
    val t = DatabaseObject.create(Token).asOysterObject
    val project = DatabaseObject.create(Project).asOysterObject
    token = t.uuid
    userUuid = user.uuid
    projectUuid = project.uuid
    t.authenticates.addLast(user)
    project.ownable.addLast(user.owner)
    user.asDatabaseObject.save()
    t.asDatabaseObject.save()
    project.asDatabaseObject.save()
  }

  "Task" should {

    "create empty task" in {
      val request = FakeRequest(POST, "/project/" + projectUuid + "/task")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty

      taskUuid = (contentAsJson(response) \ "uuid").as[String]
    }

    "get task actions" in {
      val request = FakeRequest(GET, "/task/" + taskUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")

      taskActions = (contentAsJson(response) \ "actions").as[List[String]]
    }

    "rename task" in {
      taskActions must contain("rename")

      val request = FakeRequest(PUT, "/task/" + taskUuid + "/rename")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name": "Testing Task"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Task"
    }

    "set task deadline" in {
      taskActions must contain("deadline")

      val request = FakeRequest(PUT, "/task/" + taskUuid + "/deadline")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"deadline": "2017-04-19"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "deadline").as[String] mustEqual "2017-04-19"
    }

    "set task brief" in {
      taskActions must contain("brief")

      val request = FakeRequest(PUT, "/task/" + taskUuid + "/brief")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"brief": "This task was created for testing purposes."}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "brief").as[String] mustEqual "This task was created for testing purposes."
    }

    "set task owner" in {
      taskActions must contain("assign")

      val request = FakeRequest(PUT, "/task/" + taskUuid + "/assign")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"owner": """" + userUuid + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val owners = (contentAsJson(response) \ "owners").as[List[JsValue]]
      owners.map(_ \ "uuid").map(_.as[String]) must contain(userUuid)
    }

    "refresh task actions" in {
      val request = FakeRequest(GET, "/task/" + taskUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")

      taskActions = (contentAsJson(response) \ "actions").as[List[String]]
    }

    "set task as completed" in {
      taskActions must contain("complete")

      val request = FakeRequest(PUT, "/task/" + taskUuid + "/complete")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val completed = (contentAsJson(response) \ "completed_at").as[LocalDateTime]
      val timeDifference = Duration.between(completed, LocalDateTime.now(Clock.systemUTC)).abs.toMillis
      // check completed happened "now" with 100 miliseconds tolerance
      timeDifference must be < 100L
    }

    "view task" in {
      val request = FakeRequest(GET, "/task/" + taskUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Task"
      (contentAsJson(response) \ "deadline").as[String] mustEqual "2017-04-19"
      (contentAsJson(response) \ "brief").as[String] mustEqual "This task was created for testing purposes."
      val owners = (contentAsJson(response) \ "owners").as[List[JsValue]]
      owners.map(_ \ "uuid").map(_.as[String]) must contain(userUuid)
      val completed = (contentAsJson(response) \ "completed_at").as[LocalDateTime]
      val timeDifference = Duration.between(completed, LocalDateTime.now(Clock.systemUTC)).abs.toMillis
      // check completed happened "now" with 1 second tolerance
      timeDifference must be < 1000L

      taskActions = (contentAsJson(response) \ "actions").as[List[String]]
    }

    "move to a project" in {
      val request = FakeRequest(PUT, "/task/" + taskUuid + "/move_to_project")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"project": """" + projectUuid + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
    }

    "is part of a project" in {
      val request = FakeRequest(GET, "/project/" + projectUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val tasks = (contentAsJson(response) \ "tasks").as[List[JsValue]]
      tasks.map(_ \ "uuid").map(_.as[String]) must contain(taskUuid)
    }

    "remove task deadline" in {
      taskActions must contain("deadline")

      val request = FakeRequest(PUT, "/task/" + taskUuid + "/deadline")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"deadline": null}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "deadline").asOpt[String] mustEqual None
    }

    "approve task" in {
      taskActions must contain("approve")

      val request = FakeRequest(PUT, "/task/" + taskUuid + "/approve")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val approved = (contentAsJson(response) \ "approved_at").as[LocalDateTime]
      val timeDifference = Duration.between(approved, LocalDateTime.now(Clock.systemUTC)).abs.toMillis
      // check approved happened "now" with 100 miliseconds tolerance
      timeDifference must be < 100L
    }

    "reopen task" in {
      taskActions must contain("reopen")

      val request = FakeRequest(PUT, "/task/" + taskUuid + "/reopen")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "completed_at").asOpt[LocalDateTime] mustEqual None
    }

    "remove task owner" in {
      taskActions must contain("assign")

      val request = FakeRequest(DELETE, "/task/" + taskUuid + "/assign")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"owner": """" + userUuid + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val owners = (contentAsJson(response) \ "owners").asOpt[List[JsValue]].getOrElse(Nil)
      owners.map(_ \ "uuid").map(_.as[String]) must not contain (userUuid)
    }

    "view reopened task" in {
      val request = FakeRequest(GET, "/task/" + taskUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Task"
      (contentAsJson(response) \ "deadline").asOpt[String] mustEqual None
      (contentAsJson(response) \ "brief").as[String] mustEqual "This task was created for testing purposes."
      val owners = (contentAsJson(response) \ "owners").asOpt[List[JsValue]].getOrElse(Nil)
      owners.map(_ \ "uuid").map(_.as[String]) must not contain(userUuid)
      (contentAsJson(response) \ "completed_at").asOpt[LocalDateTime] mustEqual None
      (contentAsJson(response) \ "approved_at").asOpt[LocalDateTime] mustEqual None

      taskActions = (contentAsJson(response) \ "actions").as[List[String]]
    }

    "archive task" in {
      taskActions must contain("archive")

      val request = FakeRequest(DELETE, "/task/" + taskUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "archived").as[Boolean] mustEqual true
    }

    "archived taks is not part of a project" in {
      val request = FakeRequest(GET, "/project/" + projectUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val tasks = (contentAsJson(response) \ "tasks").asOpt[List[JsValue]].getOrElse(Nil)
      tasks.map(_ \ "uuid").map(_.as[String]) must not contain (taskUuid)
    }
  }
}
