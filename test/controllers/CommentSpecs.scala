package controllers

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import org.junit.Before
import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.Database
import play.api.db.evolutions.SimpleEvolutionsReader
import play.api.db.evolutions.Evolution
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import models.DatabaseObject
import models.objects.User
import models.objects.Token
import models.objects.Project
import models.objects.Task
import play.api.libs.json.JsValue

class CommentSpecs extends PlaySpec with OneAppPerSuite with BeforeAndAfterAll {

  var token = ""
  var userUuid = ""
  var taskUuid = ""
  var comment1Uuid = ""
  var comment2Uuid = ""

  // Create testing objects
  override def beforeAll {
    implicit val db = app.injector.instanceOf[Database]
    val user = DatabaseObject.create(User).asOysterObject
    val token = DatabaseObject.create(Token).asOysterObject
    val task = DatabaseObject.create(Task).asOysterObject
    userUuid = user.uuid
    this.token = token.uuid
    taskUuid = task.uuid
    token.authenticates.addLast(user)
    user.asDatabaseObject.save()
    token.asDatabaseObject.save()
    task.asDatabaseObject.save()
  }

  "Comment" should {

    "comment on a task" in {
      val request = FakeRequest(POST, "/task/" + taskUuid + "/comment")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"brief": "Testing comment 1"}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty

      comment1Uuid = (contentAsJson(response) \ "uuid").as[String]
    }

    "view task's first comment" in {
      val request = FakeRequest(GET, "/task/" + taskUuid + "/comments")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val comments = (contentAsJson(response) \ "comments").as[List[JsValue]]
      comments.size mustBe 1
      val firstComment = comments(0)
      (firstComment \ "brief").as[String] mustEqual "Testing comment 1"
      (firstComment \ "owners").as[List[JsValue]].map(_ \ "uuid").map(_.as[String]) mustEqual List(userUuid)
    }

    "comment on a task for the second time" in {
      val request = FakeRequest(POST, "/task/" + taskUuid + "/comment")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"brief": "Testing comment 2"}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty

      comment2Uuid = (contentAsJson(response) \ "uuid").as[String]
    }

    "view task's first two comments" in {
      val request = FakeRequest(GET, "/task/" + taskUuid + "/comments")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val comments = (contentAsJson(response) \ "comments").as[List[JsValue]]
      comments.size mustBe 2
      val firstComment = comments(0)
      val secondComment = comments(1)
      (firstComment \ "brief").as[String] mustEqual "Testing comment 1"
      (firstComment \ "owners").as[List[JsValue]].map(_ \ "uuid").map(_.as[String]) mustEqual List(userUuid)
      (secondComment \ "brief").as[String] mustEqual "Testing comment 2"
      (secondComment \ "owners").as[List[JsValue]].map(_ \ "uuid").map(_.as[String]) mustEqual List(userUuid)
    }
  }
}
