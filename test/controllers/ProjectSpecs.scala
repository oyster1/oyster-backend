package controllers

import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import org.junit.Before
import play.api.db.DBApi
import play.api.db.evolutions.Evolutions
import play.api.db.Database
import play.api.db.evolutions.SimpleEvolutionsReader
import play.api.db.evolutions.Evolution
import org.scalatest.BeforeAndAfter
import org.scalatest.BeforeAndAfterAll
import models.DatabaseObject
import models.objects.User
import models.objects.Token
import models.objects.Project
import play.api.libs.json.JsValue
import play.api.libs.json.JsValue

class ProjectSpecs extends PlaySpec with OneAppPerSuite with BeforeAndAfterAll {

  var token = ""
  var userUuid = ""
  var user2Uuid = ""
  var projectUuid = ""
  var taskUuid = ""
  var projectActions = List.empty[String]

  // Create testing objects
  override def beforeAll {
    implicit val db = app.injector.instanceOf[Database]
    val user = DatabaseObject.create(User).asOysterObject
    val user2 = DatabaseObject.create(User).asOysterObject
    val t = DatabaseObject.create(Token).asOysterObject
    token = t.uuid
    userUuid = user.uuid
    user2Uuid = user2.uuid
    t.authenticates.addLast(user)
    user.asDatabaseObject.save()
    user2.asDatabaseObject.save()
    t.asDatabaseObject.save()
  }

  "Project" should {

    "create empty project" in {
      val request = FakeRequest(POST, "/project")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty

      projectUuid = (contentAsJson(response) \ "uuid").as[String]
      projectActions = (contentAsJson(response) \ "actions").as[List[String]]
    }

    "current user is project owner" in {
      projectActions must contain("assign")

      val request = FakeRequest(GET, "/project/" + projectUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val owners = (contentAsJson(response) \ "owners").as[List[JsValue]]
      val ownerUuids = owners.map(_ \ "uuid").map(_.as[String])
      ownerUuids must contain(userUuid)
    }

    "rename project" in {
      projectActions must contain("rename")

      val request = FakeRequest(PUT, "/project/" + projectUuid + "/rename")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name": "Testing Project"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Project"
    }

    "set project deadline" in {
      projectActions must contain("deadline")

      val request = FakeRequest(PUT, "/project/" + projectUuid + "/deadline")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"deadline": "2017-04-21"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "deadline").as[String] mustEqual "2017-04-21"
    }

    "set project brief" in {
      projectActions must contain("brief")

      val request = FakeRequest(PUT, "/project/" + projectUuid + "/brief")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"brief": "Short brief of Testing Project."}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "brief").as[String] mustEqual "Short brief of Testing Project."
    }

    "add project owner" in {
      projectActions must contain("assign")

      val request = FakeRequest(PUT, "/project/" + projectUuid + "/assign")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"owner": """" + user2Uuid + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val owners = (contentAsJson(response) \ "owners").as[List[JsValue]]
      owners.map(_ \ "uuid").map(_.as[String]) must contain(user2Uuid)
    }

    "add task to the project" in {
      projectActions must contain("task")

      val request = FakeRequest(POST, "/project/" + projectUuid + "/task")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"name": "Task in Testing Project", "brief": "Created in Testing Project",""" +
          """ "deadline": "2017-05-13"}""")
      val response = route(app, request).get

      status(response) mustBe 201
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "uuid").as[String] must not be empty
      (contentAsJson(response) \ "name").as[String] mustEqual "Task in Testing Project"
      (contentAsJson(response) \ "brief").as[String] mustEqual "Created in Testing Project"
      (contentAsJson(response) \ "deadline").as[String] mustEqual "2017-05-13"

      taskUuid = (contentAsJson(response) \ "uuid").as[String]
    }

    "view project" in {
      val request = FakeRequest(GET, "/project/" + projectUuid)
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Project"
      (contentAsJson(response) \ "deadline").as[String] mustEqual "2017-04-21"
      (contentAsJson(response) \ "brief").as[String] mustEqual "Short brief of Testing Project."
      val owners = (contentAsJson(response) \ "owners").as[List[JsValue]]
      val ownerUuids = owners.map(_ \ "uuid").map(_.as[String])
      ownerUuids must contain(userUuid)
      ownerUuids must contain(user2Uuid)
      val tasks = (contentAsJson(response) \ "tasks").as[List[JsValue]]
      tasks.map(_ \ "uuid").map(_.as[String]) mustEqual List(taskUuid)
    }

    "remove second user as a project owner" in {
      projectActions must contain("assign")

      val request = FakeRequest(DELETE, "/project/" + projectUuid + "/assign")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"owner": """" + user2Uuid + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val owners = (contentAsJson(response) \ "owners").as[List[JsValue]]
      val ownersUuids = owners.map(_ \ "uuid").map(_.as[String])
      ownersUuids must contain(userUuid)
      ownersUuids must not contain (user2Uuid)
    }

    "disallow removing last project owner" in {
      projectActions must contain("assign")

      val request = FakeRequest(DELETE, "/project/" + projectUuid + "/assign")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"owner": """" + userUuid + """"}""")
      val response = route(app, request).get

      status(response) mustBe 409
    }

    "readd second user as a project owner" in {
      projectActions must contain("assign")

      val request = FakeRequest(PUT, "/project/" + projectUuid + "/assign")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"owner": """" + user2Uuid + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val owners = (contentAsJson(response) \ "owners").as[List[JsValue]]
      val ownersUuids = owners.map(_ \ "uuid").map(_.as[String])
      ownersUuids must contain(userUuid)
      ownersUuids must contain(user2Uuid)
    }

    "remove self as a project owner" in {
      projectActions must contain("assign")

      val request = FakeRequest(DELETE, "/project/" + projectUuid + "/assign")
        .withHeaders("Authorization-Token" -> token)
        .withHeaders("Content-Type" -> "application/json")
        .withBody("""{"owner": """" + userUuid + """"}""")
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      val owners = (contentAsJson(response) \ "owners").as[List[JsValue]]
      val ownersUuids = owners.map(_ \ "uuid").map(_.as[String])
      ownersUuids must not contain (userUuid)
      ownersUuids must contain(user2Uuid)
    }

    "view project with tasks" in {
      val request = FakeRequest(GET, "/project/" + projectUuid + "/tasks")
        .withHeaders("Authorization-Token" -> token)
      val response = route(app, request).get

      status(response) mustBe 200
      contentType(response) mustBe Some("application/json")
      (contentAsJson(response) \ "name").as[String] mustEqual "Testing Project"
      (contentAsJson(response) \ "deadline").as[String] mustEqual "2017-04-21"
      (contentAsJson(response) \ "brief").as[String] mustEqual "Short brief of Testing Project."
      val owners = (contentAsJson(response) \ "owners").as[List[JsValue]]
      val ownerUuids = owners.map(_ \ "uuid").map(_.as[String])
      ownerUuids must not contain (userUuid)
      ownerUuids must contain(user2Uuid)
      val tasks = (contentAsJson(response) \ "tasks").as[List[JsValue]]
      tasks.size mustEqual 1
      (tasks(0) \ "uuid").as[String] mustEqual taskUuid
      (tasks(0) \ "name").as[String] mustEqual "Task in Testing Project"
      (tasks(0) \ "brief").as[String] mustEqual "Created in Testing Project"
      (tasks(0) \ "deadline").as[String] mustEqual "2017-05-13"
    }
  }
}
